//
//  XMUInterface.m
//  XMUnityWrapper
//
//  Created by XMediate on 24/08/17.
//  Copyright © 2017 XMediate. All rights reserved.

#import "XMSDK.h"
#import "XMUAdType.h"
#import "XMUBannerAdView.h"
#import "XMUFullScreenAd.h"
#import "XMURewardedVideoAd.h"
#import "XMUAdSettings.h"

static NSString* const XMUStringFromUTF8String(const char* bytes)
{
    return bytes ? @(bytes) : nil;
}

static NSString* const XMUStringFromBool(bool value)
{
    return value ? @"YES" : @"NO";
}

#pragma mark - XMediate Initialize

void XMUInitialize(const char *pubId, const char *appId)
{
    [XMSDK initializeWithPublisherId:XMUStringFromUTF8String(pubId) andAppId:XMUStringFromUTF8String(appId)];
    
    NSLog(@"XMUInitialize with pubID %@ appID %@", XMUStringFromUTF8String(pubId), XMUStringFromUTF8String(appId));
}

void XMUUpdateGDPRSettings(bool isGDPRCountry, bool wasGDPRAccepted)
{
    [XMSDK updateGDPRSettingsWithCountry:isGDPRCountry consent:wasGDPRAccepted];
    
    NSLog(@"XMUUpdateGDPRSettings with country %@ consent %@", XMUStringFromBool(isGDPRCountry), XMUStringFromBool(wasGDPRAccepted));
}

#pragma mark - Banner

XMUTypeBannerRef XMUCreateBannerViewWithSizeAndPosition(XMUTypeBannerClientRef* bannerClient, XMUAdSize adSize, XMUAdPosition position)
{
    NSLog(@"Invoke XMUCreateBannerViewWithClient");
    
    XMUBannerAdView* banner = [[XMUBannerAdView alloc] initWithBannerClientReference:bannerClient adSize:adSize andPostion:position];
    XMUTypeBannerRef bannerRef = (__bridge_retained XMUTypeBannerRef)banner;
    
    if (!bannerRef) {
        NSLog(@"XMUCreateBannerViewWithClient : XMUTypeBannerRef is nil");
    }
    
    return bannerRef;
}

XMUTypeBannerRef XMUCreateBannerViewWithSize(XMUTypeBannerClientRef* bannerClient, XMUAdSize adSize)
{
    NSLog(@"XMUCreateBannerViewWithClient");
    
    XMUBannerAdView* banner = [[XMUBannerAdView alloc] initWithBannerClientReference:bannerClient adSize:adSize andPostion:kXMAdPositionBottom];
    XMUTypeBannerRef bannerRef= (__bridge_retained XMUTypeBannerRef)banner;
    
    if (!bannerRef) {
        NSLog(@"XMUCreateBannerViewWithClient : XMUTypeBannerRef is nil");
    }
    
    return bannerRef;
}

void XMUSetBannerCallbacks(XMUTypeBannerRef bannerRef,
                           XMUBannerDidLoadAdCallback adReceivedCallback,
                           XMUBannerDidFailToLoadAdWithErrorCallback adFailedCallback,
                           XMUBannerWillPresentScreenCallback willPresentCallback,
                           XMUBannerDidDismissScreenCallback didDismissCallback,
                           XMUBannerWillLeaveApplicationCallback willLeaveCallback)
{
    NSLog(@"XMU : Set Banner Callbacks");
    
    if (!bannerRef) {
        NSLog(@"XMU : XMUTypeBannerRef is nil");
    }
    
    XMUBannerAdView* banner = (__bridge XMUBannerAdView *)bannerRef;
    if (!banner) {
        NSLog(@"XMU : banner is nil. Can't set calbbacks");
    }
    
    banner.adLoadedCallback = adReceivedCallback;
    banner.adFailedToLoadCallback = adFailedCallback;
    banner.willPresentCallback = willPresentCallback;
    banner.didDismissCallback = didDismissCallback;
    banner.willLeaveCallback = willLeaveCallback;
}

void XMULoadBannerAd(XMUTypeBannerRef bannerRef, XMUTypeAdSettingsRef adSettingRef)
{
    NSLog(@"XMU : Load Banner Ad");
    
    if (!bannerRef) {
        NSLog(@"XMU : XMUTypeBannerRef is nil");
    }
    
    XMUBannerAdView* banner = (__bridge XMUBannerAdView *)bannerRef;
    XMUAdSettings* adSettings = (__bridge XMUAdSettings *)adSettingRef;
    
    if (banner) {
        [banner loadWith:adSettings];
    } else {
        NSLog(@"XMU : banner is null. Can't load ad.");
    }
}

void XMURemoveBannerView(XMUTypeBannerRef bannerRef)
{
    NSLog(@"XMU : Remove Banner Ad");
    
    XMUBannerAdView* banner = (__bridge_transfer XMUBannerAdView *)bannerRef;
    banner = nil;
}

#pragma mark - Fullscreen

XMUTypeFullScreenAdRef XMUCreateFullScreenAd(XMUTypeFullScreenAdClientRef* client)
{
    NSLog(@"XMU : Create Fullscreen Ad");
    
    XMUFullScreenAd* fullscreenAd = [[XMUFullScreenAd alloc] initWithFullScreenAdClientReference:client];
    XMUTypeFullScreenAdRef fullscreenAdRef = (__bridge_retained XMUTypeFullScreenAdRef)fullscreenAd;
    
    if (!fullscreenAdRef) {
        NSLog(@"XMU : Fullscreen reference is nil");
    }
    
    return fullscreenAdRef;
}

void XMURemoveFullScreenAd(XMUTypeFullScreenAdRef fullscreenAdRef)
{
    XMUFullScreenAd* fullscreenAd = (__bridge_transfer XMUFullScreenAd* )fullscreenAdRef;
    fullscreenAd = nil;
}

void XMUSetFullScreenAdCallbacks(XMUTypeFullScreenAdRef fullscreenAdRef,
                                 XMUFullScreenAdDidLoadAdCallback adLoadedCallback,
                                 XMUFullScreenAdDidFailToLoadAdWithErrorCallback adFailedToLoadCallback,
                                 XMUFullScreenAdWillPresentCallback willPresentCallback,
                                 XMUFullScreenAdDidPresentCallback didPresentCallback,
                                 XMUFullScreenAdWillDismissCallback willDismissCallback,
                                 XMUFullScreenAdDidDismissCallback didDismissCallback,
                                 XMUFullScreenAdDidReceiveTapEventCallback didReceiveTapEventCallback,
                                 XMUFullScreenAdWillLeaveApplicationCallback willLeaveApplicationCallback,
                                 XMUFullScreenAdDidExpireCallback didExpireCallback,
                                 XMUFullScreenAdStartPlayingVideoCallback startPlayingVideoCallback,
                                 XMUFullScreenAdDidFailToPlayWithErrorCallback didFailToPlayVideoCallback,
                                 XMUFullScreenAdDidFinishPlayingVideoCallback didFinishPlayingVideoCallback)
{
    NSLog(@"XMU : Set Fullscreen Ad CallBacks");
    
    XMUFullScreenAd* fullscreenAd = (__bridge XMUFullScreenAd *)fullscreenAdRef;
    fullscreenAd.adLoadedCallback = adLoadedCallback;
    fullscreenAd.adFailedToLoadCallback = adFailedToLoadCallback;
    fullscreenAd.willPresentCallback = willPresentCallback;
    fullscreenAd.didPresentCallback = didPresentCallback;
    fullscreenAd.willDismissCallback = willDismissCallback;
    fullscreenAd.didReceiveTapEventCallback = didReceiveTapEventCallback;
    fullscreenAd.willLeaveApplicationCallback = willLeaveApplicationCallback;
    fullscreenAd.didExpireCallback = didExpireCallback;
    fullscreenAd.startPlayingVideoCallback = startPlayingVideoCallback;
    fullscreenAd.didFailToPlayVideoCallback = didFailToPlayVideoCallback;
    fullscreenAd.didFinishPlayingVideoCallback = didFinishPlayingVideoCallback;
}

void XMUShowFullScreenAd(XMUTypeFullScreenAdRef fullscreenAdRef)
{
    NSLog(@"XMU : Show Fullscreen Ad");
    
    XMUFullScreenAd* fullscreenAd = (__bridge XMUFullScreenAd *)fullscreenAdRef;
    [fullscreenAd show];
}

void XMULoadFullScreenAd(XMUTypeFullScreenAdRef fullscreenAdRef, XMUTypeAdSettingsRef adSettingRef)
{
    NSLog(@"XMU : Load Fullscreen Ad");
    
    XMUAdSettings* adSettings = (__bridge XMUAdSettings *)adSettingRef;
    XMUFullScreenAd* fullscreenAd = (__bridge XMUFullScreenAd *)fullscreenAdRef;
    
    [fullscreenAd loadAdWithSettings:adSettings];
}

#pragma mark - Rewarded Video Ad

XMUTypeRewardedVideoAdRef XMUCreateRewardedVideoAd(XMUTypeRewardedVideoAdClientRef* client)
{
    XMURewardedVideoAd* rewardedVideoAd = [[XMURewardedVideoAd alloc] initWithRewardedVideoAdClientReference:client];
    XMUTypeRewardedVideoAdRef rewardedVideoAdRef = (__bridge_retained XMUTypeRewardedVideoAdRef)rewardedVideoAd;
    
    if (!rewardedVideoAdRef) {
        NSLog(@"XMU : rewarded video ref is nil");
    }
    
    return rewardedVideoAdRef;
}

void XMURemoveRewardedVideoAd(XMUTypeRewardedVideoAdRef rewardedVideoRef)
{
    XMURewardedVideoAd* rewardedVideoAd = (__bridge_transfer XMURewardedVideoAd* )rewardedVideoRef;
    rewardedVideoAd = nil;
}

void XMUSetRewardedVideoAdCallbacks(XMUTypeRewardedVideoAdRef rewardedVideoRef,
                                    XMURewardedVideoAdDidLoadAdCallback adLoadedCallback,
                                    XMURewardedVideoAdDidFailToLoadAdWithErrorCallback adFailedCallback,
                                    XMURewardedVideoAdWillPresentCallback willPresentCallback,
                                    XMURewardedVideoAdDidPresentCallback didPresentCallback,
                                    XMURewardedVideoAdWillDismissCallback willDismissCallback,
                                    XMURewardedVideoAdDidDismissCallback didDismissCallback,
                                    XMURewardedVideoAdDidStartPlayingCallback didStartPlayingCallback,
                                    XMURewardedVideoAdDidFailToPlayWithErrorCallback didFailedToPlayCallback,
                                    XMURewardedVideoAdDidExpireCallback didExpireCallback,
                                    XMURewardedVideoAdDidReceiveTapEventCallback didReceiveTapEventCallback,
                                    XMURewardedVideoAdWillLeaveApplicationCallback willLeaveCallback,
                                    XMURewardedVideoAdDidReceivedRewardCallback didReceiveRewardCallback)
{
    NSLog(@"XMU : Set Rewarded Video CallBacks");
    
    XMURewardedVideoAd* rewardedVideoAd = (__bridge XMURewardedVideoAd *)rewardedVideoRef;
    rewardedVideoAd.adLoadedCallback = adLoadedCallback;
    rewardedVideoAd.adFailedToLoadCallback = adFailedCallback;
    rewardedVideoAd.willPresentCallback = willPresentCallback;
    rewardedVideoAd.didPresentCallback = didPresentCallback;
    rewardedVideoAd.willDismissCallback = willDismissCallback;
    rewardedVideoAd.didDismissCallback = didDismissCallback;
    rewardedVideoAd.didStartPlayingCallback = didStartPlayingCallback;
    rewardedVideoAd.didReceivedRewardCallback = didReceiveRewardCallback;
    rewardedVideoAd.didFailedToPlayCallback = didFailedToPlayCallback;
    rewardedVideoAd.didExpireCallback = didExpireCallback;
    rewardedVideoAd.didReceiveTapEventCallback = didReceiveTapEventCallback;
    rewardedVideoAd.willLeaveCallback = willLeaveCallback;
}

void XMUShowRewardedVideoAd(XMUTypeRewardedVideoAdRef rewardedVideoRef)
{
    NSLog(@"XMU : Show Rewarded Video Ad");
    
    XMURewardedVideoAd* rewardedVideoAd = (__bridge XMURewardedVideoAd *)rewardedVideoRef;
    [rewardedVideoAd show];
}

void XMULoadRewardedVideoAd(XMUTypeRewardedVideoAdRef rewardedVideoRef, XMUTypeAdSettingsRef adSettingRef)
{
    NSLog(@"XMU : Load Rewarded Video Ad");
    
    XMURewardedVideoAd* rewardedVideoAd = (__bridge XMURewardedVideoAd *)rewardedVideoRef;
    XMUAdSettings* adSettings = (__bridge XMUAdSettings *)adSettingRef;
    
    [rewardedVideoAd loadAdWithSetting:adSettings];
}

#pragma mark - Settings

XMUTypeAdSettingsRef XMUCreateAdSetting()
{
    XMUAdSettings* adSetting = [XMUAdSettings new];
    return (__bridge_retained XMUTypeAdSettingsRef)adSetting;
}

void XMURemoveAdSetting(XMUTypeAdSettingsRef adSettingRef)
{
    XMUAdSettings* adSetting = (__bridge_transfer XMUAdSettings *)adSettingRef;
    adSetting = nil;
}

void XMUSetGender(XMUTypeAdSettingsRef adSettingRef, NSInteger gender)
{
    XMUAdSettings* adSettings = (__bridge XMUAdSettings *)adSettingRef;
    [adSettings addGender:gender];
}

void XMUSetMaritalStatus(XMUTypeAdSettingsRef adSettingRef, NSInteger maritalStatus)
{
    XMUAdSettings* adSettings = (__bridge XMUAdSettings *)adSettingRef;
    [adSettings addMaritalStatus:maritalStatus];
}

void XMUSetSexualOrientation(XMUTypeAdSettingsRef adSettingRef, NSInteger orientation) {
    XMUAdSettings* adSettings = (__bridge XMUAdSettings *)adSettingRef;
    [adSettings addSexualOrientation:orientation];
}

void XMUSetAge(XMUTypeAdSettingsRef adSettingRef, NSInteger age) {
    XMUAdSettings* adSettings = (__bridge XMUAdSettings *)adSettingRef;
    [adSettings addAge:age];
}

void XMUSetIncome(XMUTypeAdSettingsRef adSettingRef, NSInteger income) {
    XMUAdSettings* adSettings = (__bridge XMUAdSettings *)adSettingRef;
    [adSettings addIncome:income];
}

void XMUSetKeywords(XMUTypeAdSettingsRef adSettingRef, const char *keywords) {
    XMUAdSettings* adSettings = (__bridge XMUAdSettings *)adSettingRef;
    [adSettings addKeywords:XMUStringFromUTF8String(keywords)];
}

void XMUSetEducation(XMUTypeAdSettingsRef adSettingRef, const char *education) {
    XMUAdSettings* adSettings = (__bridge XMUAdSettings *)adSettingRef;
    [adSettings addEducation:XMUStringFromUTF8String(education)];
}

void XMUSetLanguage(XMUTypeAdSettingsRef adSettingRef, const char *language) {
    XMUAdSettings* adSettings = (__bridge XMUAdSettings *)adSettingRef;
    [adSettings addLanguage:XMUStringFromUTF8String(language)];
}

void XMUSetAreaCode(XMUTypeAdSettingsRef adSettingRef, const char *areaCode) {
    XMUAdSettings* adSettings = (__bridge XMUAdSettings *)adSettingRef;
    [adSettings addAreaCode:XMUStringFromUTF8String(areaCode)];
}

void XMUSetTesting(XMUTypeAdSettingsRef adSettingRef, bool isTesting) {
    XMUAdSettings* adSettings = (__bridge XMUAdSettings *)adSettingRef;
    [adSettings addTesting:isTesting];
}

void XMUSetBirthday(XMUTypeAdSettingsRef adSettingRef, NSInteger year, NSInteger month, NSInteger day) {
    XMUAdSettings* adSettings = (__bridge XMUAdSettings *)adSettingRef;
    [adSettings addDateOfBirthWithMonth:month day:day year:year];
}


void XMUSetLocation(XMUTypeAdSettingsRef settings, double latitude, double longitude) {
    XMUAdSettings *xmuAdSettings = (__bridge XMUAdSettings *)settings;
    [xmuAdSettings addLocationWithLatitude:latitude longitude:longitude];
}

