//
//  XMUUtil.h
//  XMUnityWrapper
//
//  Created by XMediate on 14/09/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMUAdType.h"
#import "XMSDK.h"
#import <UIKit/UIKit.h>

@interface XMUUtil : NSObject

+ (UIViewController *)unityViewController;
+ (void)positionView:(UIView *)view inParentBounds:(CGRect)parentBounds adPosition:(XMUAdPosition)adPosition;

@end
