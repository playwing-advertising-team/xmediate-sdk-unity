//
//  XMFullScreenAdDelegate.h
//  XMediateSDK
//
//  Created by XMediate on 29/09/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
@class XMFullScreenAd;

@protocol XMFullScreenAdDelegate <NSObject>

/**
 * Tells the delegate that a FullScreenAd was received.
 *
 * @param fullScreenAd The fullscreen ad object.
 */
- (void)fullScreenAdDidLoad:(XMFullScreenAd *)fullScreenAd;

/**
 * Tells the delegate that the fullscreen ad failed to load.
 *
 * @param fullScreenAd The fullscreen ad object.
 * @param error An error indicating why the ad failed to load.
 */
- (void)fullScreenAd:(XMFullScreenAd *)fullScreenAd didFailToLoadWithError:(NSError *)error;

/**
 * This method is called when a fullscreen ad is about to appear.
 *
 * @param fullScreenAd The  fullscreen ad object.
 */
- (void)fullScreenAdWillPresent:(XMFullScreenAd *)fullScreenAd;

/**
 * This method is called when a  fullscreen ad has appeared.
 *
 * @param fullScreenAd The  fullscreen ad object.
 */
- (void)fullScreenAdDidPresent:(XMFullScreenAd *)fullScreenAd;

/**
 * This method is called when a  fullscreen ad will be dismissed.
 *
 * @param fullScreenAd The  fullscreen ad object.
 */
- (void)fullScreenWillDismiss:(XMFullScreenAd *)fullScreenAd;

/**
 * This method is called when a  fullscreen ad has been dismissed.
 *
 * @param fullScreenAd The  fullscreen ad object.
 */
- (void)fullScreenDidDismiss:(XMFullScreenAd *)fullScreenAd;

/**
 * This method is called when the user taps on the ad.
 *
 * @param fullScreenAd The  fullscreen ad object.
 */
- (void)fullScreenDidReceiveTapEvent:(XMFullScreenAd *)fullScreenAd;

/**
 * This method is called when a  fullscreen ad will cause the user to leave the application.
 *
 * @param fullScreenAd The  fullscreen ad object.
 */
- (void)fullScreenWillLeaveApplication:(XMFullScreenAd *)fullScreenAd;

/**
 * This method is called when a previously loaded  fullscreen is no longer eligible for presentation.
 *
 * @param fullScreenAd The  fullscreen ad object.
 */
- (void)fullScreenAdDidExpire:(XMFullScreenAd *)fullScreenAd;
@end
