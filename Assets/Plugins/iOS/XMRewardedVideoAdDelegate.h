//
//  XMRewardedVideoAdDelegate.h
//  XMediateSDK
//
//  Created by XMediate on 23/03/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
@class XMRewardedVideoAd;
@class XMRewardedVideoReward;

@protocol XMRewardedVideoAdDelegate <NSObject>

/**
 * Tells the delegate that a rewarded video ad was received.
 *
 * @param rewardedVideoAd The rewarded video ad object.
 */
- (void)rewardVideoAdDidLoad:(XMRewardedVideoAd *)rewardedVideoAd;

/**
 * Tells the delegate that the rewarded video ad failed to load.
 *
 * @param rewardedVideoAd The rewarded video ad object.
 * @param error An error indicating why the ad failed to load.
 */
- (void)rewardedVideoAd:(XMRewardedVideoAd *)rewardedVideoAd
    didFailToLoadWithError:(NSError *)error;

/**
 * This method is called when a rewarded video ad is about to appear.
 *
 * @param rewardedVideoAd The rewarded video ad object.
 */
- (void)rewardedVideoAdWillPresent:(XMRewardedVideoAd *)rewardedVideoAd;

/**
 * This method is called when a rewarded video ad has appeared.
 *
 * @param rewardedVideoAd The rewarded video ad object.
 */
- (void)rewardedVideoAdDidPresent:(XMRewardedVideoAd *)rewardedVideoAd;


/**
 * Tells the delegate that the rewarded video ad started playing.
 *
 * @param rewardedVideoAd The rewarded video ad object.
 */
- (void)rewardedVideoAdDidStartPlaying:(XMRewardedVideoAd *)rewardedVideoAd;


/**
 * This method is called when a rewarded video fails to play .
 *
 * @param rewardedVideoAd The rewarded video ad object.
 * @param error An error describing why the video couldn't play.
 */
- (void)rewardedVideoAd:(XMRewardedVideoAd *)rewardedVideoAd didFailToPlayWithError:(NSError *)error;


/**
 * This method is called when a rewarded video ad will be dismissed.
 *
 * @param rewardedVideoAd The rewarded video ad object.
 */
- (void)rewardedVideoAdWillDismiss:(XMRewardedVideoAd *)rewardedVideoAd;

/**
 * This method is called when a rewarded video ad has been dismissed.
 *
 * @param rewardedVideoAd The rewarded video ad object.
 */
- (void)rewardedVideoAdDidDismiss:(XMRewardedVideoAd *)rewardedVideoAd;

/**
 * This method is called when the user taps on the ad.
 *
 * @param rewardedVideoAd The rewarded video ad object.
 */
- (void)rewardedVideoAdDidReceiveTapEvent:(XMRewardedVideoAd *)rewardedVideoAd;

/**
 * This method is called when a rewarded video ad will cause the user to leave the application.
 *
 * @param rewardedVideoAd The rewarded video ad object.
 */
- (void)rewardedVideoAdWillLeaveApplication:(XMRewardedVideoAd *)rewardedVideoAd;

/**
 * Tells the delegate that the rewarded video ad has rewarded the user.
 *
 * @param rewardedVideoAd The rewarded video ad object.
 * @param reward The object that contains all the information regarding how much you should reward the user.
 */

- (void)rewardedVideoAd:(XMRewardedVideoAd *)rewardedVideoAd
   didRewardUserWithReward:(XMRewardedVideoReward *)reward;

/**
 * This method is called when a previously loaded rewarded video is no longer eligible for presentation.
 *
 * @param rewardedVideoAd The rewarded video ad object.
 */
- (void)rewardedVideoAdDidExpire:(XMRewardedVideoAd *)rewardedVideoAd;

@end
