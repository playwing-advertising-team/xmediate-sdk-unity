//
//  XMBannerAdView.h
//  XMediateSDK
//
//  Created by XMediate on 31/01/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMAdSize.h"
#import "XMAdSettings.h"

@protocol XMBannerAdViewDelegate;
/**
 * The XMBannerAdView class provides a view that can be used to display banner advertisements.
 * Applications should create this view and place it in the view heirarchy.
 */

@interface XMBannerAdView : UIView

- (id)initWithSize:(XMAdSize)size viewControllerForPresentingModal:(UIViewController*)presentingController;

- (id)initWithSize:(XMAdSize)size viewControllerForPresentingModal:(UIViewController*)presentingController delegate:(id<XMBannerAdViewDelegate>)delegate;

/// banner refresh interval - 30 to 90 seconds. Set to 0 to disable refresh.
- (void)setBannerRefreshInterval:(NSUInteger)interval;


/**
 * The delegate (`XMBannerAdViewDelegate`) of the XMBannerAdView instance.
 *
 */
@property (nonatomic, weak) id<XMBannerAdViewDelegate> delegate;

@property(nonatomic, weak) UIViewController *viewControllerForPresentingModal;

/**
 * Requests a new ad.
 *
 * If the ad view is already loading an ad, this call will be ignored.
 */
-(void) loadWithSettings:(XMAdSettings*)adSettings;


@end
