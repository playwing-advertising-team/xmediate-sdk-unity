//
//  XMUBannerAdView.m
//  XMUnityWrapper
//
//  Created by XMediate on 24/08/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import "XMUBannerAdView.h"
#import "XMSDK.h"
#import "XMUUtil.h"

@interface XMUBannerAdView ()<XMBannerAdViewDelegate>
@property (nonatomic, assign) XMUAdPosition pos;
@end

@implementation XMUBannerAdView

- (instancetype)initWithBannerClientReference:(XMUTypeBannerClientRef *)bannerClient adSize:(XMUAdSize)size andPostion:(XMUAdPosition)position
{
    self = [super init];
    
    if (self) {
        self.bannerClient = bannerClient;
        self.pos = position;
        XMAdSize xmAdSize = [self getXMAdSizeFrom:size];
        self.bannerView = [[XMBannerAdView alloc] initWithSize:xmAdSize viewControllerForPresentingModal:[XMUUtil unityViewController] delegate:self];
    }
    
    return self;
}

- (void)dealloc
{
    [self.bannerView removeFromSuperview];
    self.bannerView = nil;
}

- (void)loadWith:(XMUAdSettings *)xmuSettings
{
    NSLog(@"Load banner ad");
    
    XMAdSettings *adSettings = [xmuSettings settings];
    [self.bannerView loadWithSettings:adSettings];
}

- (XMAdSize)getXMAdSizeFrom:(XMUAdSize)size
{
    switch (size) {
        case kXMUBanner: return XM_BANNER_SIZE;
        case kXMUMediumRectangle: return XM_MEDIUM_RECT_SIZE;
        case KXMSkyScrapperSize: return XM_SKYSCRAPER_SIZE;
        case kXMULeaderBoardSize: return XM_LEADERBOARD_SIZE;
        case KXMWideSkyScrapperSize: return XM_WIDE_SKYSCRAPER_SIZE;
        default: return XM_BANNER_SIZE;
    }
}

#pragma mark - XMBannerAdViewDelegate callbacks

- (void)bannerAdViewDidLoadAd:(XMBannerAdView *)view
{
    NSLog(@"XMediate : Banner Ad View Loaded.");
    
    [self.bannerView removeFromSuperview];
    
    UIViewController* unityController = [XMUUtil unityViewController];
    UIView* unityView = unityController.view;
    
    self.bannerView = view;
    
    [XMUUtil positionView:self.bannerView inParentBounds:unityView.bounds adPosition:self.pos];
    [unityView addSubview:self.bannerView];
    
    if(self.adLoadedCallback){
        self.adLoadedCallback(self.bannerClient);
    } else {
        NSLog(@"callback is nil");
    }
}

- (void)bannerAdViewDidFailToLoadAd:(XMBannerAdView *)view withError:(NSError *)error
{
    NSLog(@"XMediate : Banner Ad View Loading failed with error: %@.",error);
    
    if (self.adFailedToLoadCallback) {
        NSString *errorMsg = [NSString stringWithFormat:@"Failed to load ad: %@", [error localizedFailureReason]];
        self.adFailedToLoadCallback(self.bannerClient, [errorMsg cStringUsingEncoding:NSUTF8StringEncoding]);
    }
}

- (void)willPresentFullScreenForAd:(XMBannerAdView *)view
{
    NSLog(@"XMediate : Banner Ad View will Present FullScreen.");
    
    if (self.willPresentCallback) {
        self.willPresentCallback(self.bannerClient);
    } else {
        NSLog(@"callback is nil");
    }
}

- (void)didDismissFullScreenForAd:(XMBannerAdView *)view
{
    NSLog(@"XMediate : Banner Ad View did dismiss FullScreen.");
    
    if (self.didDismissCallback) {
        self.didDismissCallback(self.bannerClient);
    } else {
        NSLog(@"callback is nil");
    }
}

- (void)willLeaveApplicationFromAd:(XMBannerAdView *)view
{
    NSLog(@"XMediate : Banner Ad View will leave application.");
    
    if (self.willLeaveCallback) {
        self.willLeaveCallback(self.bannerClient);
    } else {
        NSLog(@"callback is nil");
    }
}

@end
