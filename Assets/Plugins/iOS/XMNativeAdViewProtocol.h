//
//  XMNativeAdPresenter.h
//  XMediateSDK
//
//  Created by XMediate on 03/07/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol XMNativeAdViewProtocol <NSObject>

-(UILabel *) nativeAdHeadLineLabel;

-(UILabel *) nativeAdBodyLabel;

-(UILabel *) nativeAdCallToActionLabel;

-(UIImageView *) nativeAdIconImage;

-(UIImageView *) nativeAdPrimaryImage;

-(UILabel *) nativeAdAdvertizerLabel;

-(UILabel *) nativeAdRatingLabel;

-(UILabel *) nativeAdStoreLabel;

-(UILabel *) nativeAdPriceLabel;

@end
