//
//  XMAdConfiguration.h
//  XMediateSDK
//
//  Created by XMediate on 17/04/17.
//  Copyright © 2017 XMediate. All rights reserved.
//



#import <Foundation/Foundation.h>

/*! @enum */
/// Genders to help deliver more relevant ads.
typedef NS_ENUM(NSInteger, XMGender) {
  XMGenderUnKnown,
  XMGenderOther,    ///Other gender.
  XMGenderMale,     ///Male gender.
  XMGenderFemale    ///Female gender.
};

typedef NS_ENUM(NSInteger,XMMaritalStatus) {
  XMMaritalStatusUnKnown,
  XMMaritalStatusMarried,    /// Married
  XMMaritalStatusUnMarried,  /// UnMarried
  XMMaritalStatusDivorced,   /// Divorced
  XMMaritalStatusWidowed      /// Widow
};


typedef NS_ENUM(NSInteger,XMSexualOrientaion) {
  XMSexualOrientaionUnKnown,
  XMSexualOrientaionStraight,    /// Straight
  XMSexualOrientaionBiSexual,    /// BiSexual
  XMSexualOrientaionHomoSexual   /// HomoSexual
};

@interface XMAdSettings : NSObject

+ (instancetype) settings;

@property(nonatomic, assign) XMGender gender;
@property(nonatomic, assign) XMMaritalStatus maritalStatus;
@property(nonatomic, assign) XMSexualOrientaion sexualOrientation;
@property(nonatomic, assign) NSUInteger age;
@property(nonatomic, assign) NSUInteger income;
@property(nonatomic, copy) NSString* education;
@property(nonatomic, copy) NSString* language;
@property(nonatomic, copy) NSString* areaCode;

/// Comma (,) separated strings. Keywords are words or phrases describing the current user activity
/// such as @"Sports Scores" or @"Football".
@property(nonatomic, copy) NSString* keywords;

/**
 * Optional NSDictionary containing any extra key-value parameters for the user. Defaults to empty.
 */
@property (nonatomic, copy) NSDictionary<NSString*,NSString*> *extraParams;

/**
 * A Boolean value that determines whether the ad view should request ads in test mode.
 *
 * The default value is NO.
 */
@property (nonatomic, assign, getter = isTesting) BOOL testing;

/**
 * When testing the ads you can provide your simulator/device's identifier. This will be forwarded to adnetworks theat need them.
 * Device hash id should be without "-" (Hyphen)
 */
@property(nonatomic, copy) NSArray<NSString*>* testDevices;


- (void)setDateOfBirthWithMonth:(NSUInteger)month
                            day:(NSUInteger)day
                           year:(NSUInteger)year;

/// The user's current location may be used to deliver more relevant ads.
/// However do not use Core Location just for advertising, make sure it is used for more beneficial reasons as well.
/// It is both a good idea and part of Apple's guidelines.
/// [TODO:] Add Accuracy here.
- (void)setLocationWithLatitude:(CGFloat)latitude
                      longitude:(CGFloat)longitude;

@end
