//
//  XMBannerAdViewDelegate.h
//  XMediateSDK
//
//  Created by XMediate on 31/01/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol XMBannerAdViewDelegate <NSObject>

@optional

/**
  - TODO: Decide whether we want to provide callback like adFetched Success and Failure.
 */

/**
 * Sent when an ad view successfully loads an ad.
 *
 * Your implementation should typically wait for this callback to insert the ad view into the view hierarchy, if you have not already done so.
 *
 * @param view The ad view sending the callback.
 */
- (void)bannerAdViewDidLoadAd:(XMBannerAdView *)view;

/**
 * Sent when an ad view fails to load an ad.
 *
 * To avoid displaying blank ads, you should hide the ad view in response to this message.
 *
 * @param view The ad view sending the message.
 */
- (void)bannerAdViewDidFailToLoadAd:(XMBannerAdView *)view withError:(NSError*)error;

/** @name Detecting When a User Interacts With the Ad View */

/**
 * Sent when an ad view is about to present modal content.
 *
 * This method is called when the user taps on the ad view. Your implementation of this method
 * should pause any application activity that requires user interaction.
 *
 * @param view The ad view sending the message.
 * @see `didDismissModalViewForAd:`
 */
- (void)willPresentFullScreenForAd:(XMBannerAdView *)view;

/**
 * Sent when an ad view has dismissed its modal content, returning control to your application.
 *
 * Your implementation of this method should resume any application activity that was paused
 * in response to `willPresentModalViewForAd:`.
 *
 * @param view The ad view sending the message.
 * @see `willPresentModalViewForAd:`
 */
- (void)didDismissFullScreenForAd:(XMBannerAdView *)view;

/**
 * Sent when a user is about to leave your application as a result of tapping
 * on an ad.
 *
 * Your application will be moved to the background shortly after this method is called.
 *
 * @param view The ad view sending the message.
 */
- (void)willLeaveApplicationFromAd:(XMBannerAdView *)view;

@end
