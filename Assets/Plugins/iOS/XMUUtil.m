//
//  XMUUtil.m
//  XMUnityWrapper
//
//  Created by XMediate on 14/09/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import "XMUUtil.h"
#import "UnityAppController.h"

@implementation XMUUtil

+ (UIViewController *)unityViewController {
    return ((UnityAppController *)[UIApplication sharedApplication].delegate).rootViewController;
}


+ (void)positionView:(UIView *)view
      inParentBounds:(CGRect)parentBounds
          adPosition:(XMUAdPosition)adPosition {
    CGPoint center = CGPointMake(CGRectGetMidX(parentBounds), CGRectGetMidY(view.bounds));
    switch (adPosition) {
        case kXMAdPositionTop:
            center = CGPointMake(CGRectGetMidX(parentBounds), CGRectGetMidY(view.bounds));
            break;
        case kXMAdPositionBottom:
            center = CGPointMake(CGRectGetMidX(parentBounds),
                                 CGRectGetMaxY(parentBounds) - CGRectGetMidY(view.bounds));
            break;
        case kXMAdPositionTopLeft:
            center = CGPointMake(CGRectGetMidX(view.bounds), CGRectGetMidY(view.bounds));
            break;
        case kXMAdPositionTopRight:
            center = CGPointMake(CGRectGetMaxX(parentBounds) - CGRectGetMidX(view.bounds),
                                 CGRectGetMidY(view.bounds));
            break;
        case kXMAdPositionBottomLeft:
            center = CGPointMake(CGRectGetMidX(view.bounds),
                                 CGRectGetMaxY(parentBounds) - CGRectGetMidY(view.bounds));
            break;
        case kXMAdPositionBottomRight:
            center = CGPointMake(CGRectGetMaxX(parentBounds) - CGRectGetMidX(view.bounds),
                                 CGRectGetMaxY(parentBounds) - CGRectGetMidY(view.bounds));
            break;
        case kXMAdPositionCenter:
            center = CGPointMake(CGRectGetMidX(parentBounds), CGRectGetMidY(parentBounds));
            break;
        default:
            break;
    }
    view.center = center;
}

@end
