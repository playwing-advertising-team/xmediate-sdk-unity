//
//  XMNativeAdLoaderDelegate.h
//  XMediateSDK
//
//  Created by Tarun Sharma on 09/06/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
@class XMNativeAd;

@protocol XMNativeAdLoaderDelegate <NSObject>
- (void)nativeAdDidLoad:(XMNativeAd *)nativeAd;
- (void)nativeAd:(XMNativeAd *)nativeAd didFailWithError:(NSError *)error;
@end
