    //
//  XMUFullScreenAd.m
//  XMediateSDK
//
//  Created by Fedor Melnichenko on 11/9/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import "XMUUtil.h"

#import "XMUFullScreenAd.h"

@interface XMUFullScreenAd () <XMFullScreenAdDelegate, XMFullScreenAdVideoPlaybackDelegate>

@end

@implementation XMUFullScreenAd

- (BOOL)isReady
{
    return self.fullScreenAd.isReady;
}

- (instancetype)initWithFullScreenAdClientReference:(XMUTypeFullScreenAdClientRef *)fullScreenAdClient
{
    self = [super init];
    
    if (self) {
        self.fullScreenAdClient = fullScreenAdClient;
        
        self.fullScreenAd = [XMFullScreenAd new];
        self.fullScreenAd.delegate = self;
        self.fullScreenAd.videoPlaybackDelegate = self;
    }
    
    return self;
}

- (void)dealloc
{
    self.fullScreenAd.delegate = nil;
    self.fullScreenAd.videoPlaybackDelegate = nil;
    
    self.fullScreenAd = nil;
}

- (void)loadAdOfType:(XMFullScreenAdType)type withSettings:(XMUAdSettings *)xmuSettings;
{
    [self.fullScreenAd loadFullScreenAdOfType:type WithSettings:xmuSettings.settings];
}

- (void)loadAdWithSettings:(XMUAdSettings *)settings
{
    [self loadAdOfType:XMFullScreenTypeDefault withSettings:settings];
}

- (void)show
{
    if (self.fullScreenAd.isReady) {
        UIViewController* controller = [XMUUtil unityViewController];
        [self.fullScreenAd presentFromViewController:controller];
    } else {
        NSLog(@"XMediate : fullscreen ad not ready yet");
    }
}

#pragma mark - XMFullScreenAdDelegate

- (void)fullScreenAdDidLoad:(XMFullScreenAd *)fullScreenAd
{
    NSLog(@"XMediate : fullscreen ad loaded");
    if (self.adLoadedCallback) {
        self.adLoadedCallback(self.fullScreenAdClient);
    } else {
        NSLog(@"XMPlugin Fullscreen : callback is null");
    }
}

- (void)fullScreenAd:(XMFullScreenAd *)fullScreenAd didFailToLoadWithError:(NSError *)error
{
    NSLog(@"XMediate : fullscreen ad failed to load");
    if (self.adFailedToLoadCallback) {
        NSString *errorMessage = [NSString stringWithFormat:@"Failed to load ad with error: %@", error.description];
        self.adFailedToLoadCallback(self.fullScreenAdClient, [errorMessage cStringUsingEncoding:NSUTF8StringEncoding]);
    } else {
        NSLog(@"XMPlugin Fullscreen : callback is null");
    }
}

- (void)fullScreenAdWillPresent:(XMFullScreenAd *)fullScreenAd
{
    NSLog(@"XMediate : fullscreen ad will present");
    if (self.willPresentCallback) {
        self.willPresentCallback(self.fullScreenAdClient);
    } else {
        NSLog(@"XMPlugin Fullscreen : callback is null");
    }
}

- (void)fullScreenAdDidPresent:(XMFullScreenAd *)fullScreenAd
{
    NSLog(@"XMediate : fullscreen ad did present");
    if (self.didPresentCallback) {
        self.didPresentCallback(self.fullScreenAdClient);
    } else {
        NSLog(@"XMPlugin Fullscreen : callback is null");
    }
}

- (void)fullScreenWillDismiss:(XMFullScreenAd *)fullScreenAd
{
    NSLog(@"XMediate : fullscreen ad will dismiss");
    if (self.willDismissCallback) {
        self.willDismissCallback(self.fullScreenAdClient);
    } else {
        NSLog(@"XMPlugin Fullscreen : callback is null");
    }
}

- (void)fullScreenDidDismiss:(XMFullScreenAd *)fullScreenAd
{
    NSLog(@"XMediate : fullscreen ad did dismiss");
    if (self.didDismissCallback) {
        self.didDismissCallback(self.fullScreenAdClient);
    } else {
        NSLog(@"XMPlugin Fullscreen : callback is null");
    }
}

- (void)fullScreenDidReceiveTapEvent:(XMFullScreenAd *)fullScreenAd
{
    NSLog(@"XMediate : fullscreen ad did receive tap");
    if (self.didReceiveTapEventCallback) {
        self.didReceiveTapEventCallback(self.fullScreenAdClient);
    } else {
        NSLog(@"XMPlugin Fullscreen : callback is null");
    }
}

- (void)fullScreenWillLeaveApplication:(XMFullScreenAd *)fullScreenAd
{
    NSLog(@"XMediate : fullscreen ad will leave application");
    if (self.willLeaveApplicationCallback) {
        self.willLeaveApplicationCallback(self.fullScreenAdClient);
    } else {
        NSLog(@"XMPlugin Fullscreen : callback is null");
    }
}

- (void)fullScreenAdDidExpire:(XMFullScreenAd *)fullScreenAd
{
    NSLog(@"XMediate : fullscreen ad did expire");
    if (self.didExpireCallback) {
        self.didExpireCallback(self.fullScreenAdClient);
    } else {
        NSLog(@"XMPlugin Fullscreen : callback is null");
    }
}

#pragma mark - XMFullScreenAdVideoPlaybackDelegate

- (void)fullScreenAdStartPlayingVideo:(XMFullScreenAd *)fullScreenAd
{
    NSLog(@"XMediate : fullscreen ad start playing");
    if (self.startPlayingVideoCallback) {
        self.startPlayingVideoCallback(self.fullScreenAdClient);
    } else {
        NSLog(@"XMPlugin Fullscreen : callback is null");
    }
}

- (void)fullScreenAd:(XMFullScreenAd *)fullScreenAd didFailToPlayWithError:(NSError *)error
{
    NSLog(@"XMediate : fullscreen ad failed to play");
    if (self.didFailToPlayVideoCallback) {
        NSString *errorMessage = [NSString stringWithFormat:@"Failed to load ad with error: %@", error.description];
        self.didFailToPlayVideoCallback(self.fullScreenAdClient, [errorMessage cStringUsingEncoding:NSUTF8StringEncoding]);
    } else {
        NSLog(@"XMPlugin Fullscreen : callback is null");
    }
}

- (void)fullScreenAdDidFinishPlayingVideo:(XMFullScreenAd *)fullScreenAd
{
    NSLog(@"XMediate : fullscreen ad did finish playing");
    if (self.didFinishPlayingVideoCallback) {
        self.didFinishPlayingVideoCallback(self.fullScreenAdClient);
    } else {
        NSLog(@"XMPlugin Fullscreen : callback is null");
    }
}

@end
