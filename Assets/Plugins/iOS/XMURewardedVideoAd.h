//
//  XMURewardedVideoAd.h
//  XMUnityWrapper
//
//  Created by XMediate on 18/09/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMUAdType.h"
#import "XMSDK.h"
#import "XMUAdSettings.h"

@interface XMURewardedVideoAd : NSObject
/// Initializes a XMURewardedVideoAd.
- (id)initWithRewardedVideoAdClientReference:(XMUTypeRewardedVideoAdClientRef *)rewardedVideoAdClient;

/// The XM video ad.
@property(nonatomic, strong) XMRewardedVideoAd *rewardedVideoAd;

@property(nonatomic, assign) XMUTypeRewardedVideoAdClientRef *rewardedVideoAdClient;

@property(nonatomic, assign) XMURewardedVideoAdDidLoadAdCallback adLoadedCallback;
@property(nonatomic, assign) XMURewardedVideoAdDidFailToLoadAdWithErrorCallback adFailedToLoadCallback;

@property(nonatomic, assign) XMURewardedVideoAdWillPresentCallback willPresentCallback;
@property(nonatomic, assign) XMURewardedVideoAdDidPresentCallback didPresentCallback;

@property(nonatomic, assign) XMURewardedVideoAdDidStartPlayingCallback didStartPlayingCallback;
@property(nonatomic, assign) XMURewardedVideoAdDidFailToPlayWithErrorCallback didFailedToPlayCallback;
@property(nonatomic, assign) XMURewardedVideoAdDidReceivedRewardCallback didReceivedRewardCallback;

@property(nonatomic, assign) XMURewardedVideoAdWillDismissCallback willDismissCallback;
@property(nonatomic, assign) XMURewardedVideoAdDidDismissCallback didDismissCallback;

@property(nonatomic, assign) XMURewardedVideoAdDidExpireCallback didExpireCallback;

@property(nonatomic, assign) XMURewardedVideoAdDidReceiveTapEventCallback didReceiveTapEventCallback;

@property(nonatomic, assign) XMURewardedVideoAdWillLeaveApplicationCallback willLeaveCallback;

- (void)loadAdWithSetting:(XMUAdSettings *)settings;
- (BOOL)isReady;
- (void)show;
@end
