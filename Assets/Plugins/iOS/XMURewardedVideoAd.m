//
//  XMURewardedVideoAd.m
//  XMUnityWrapper
//
//  Created by XMediate on 18/09/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import "XMURewardedVideoAd.h"
#import "XMUUtil.h"

@interface XMURewardedVideoAd () <XMRewardedVideoAdDelegate>

@end

@implementation XMURewardedVideoAd

-(id) initWithRewardedVideoAdClientReference:(XMUTypeRewardedVideoAdClientRef *)client{
    self = [super init];
    if (self) {
        self.rewardedVideoAdClient = client;
        self.rewardedVideoAd = [[XMRewardedVideoAd alloc] initWithDelegate:self];
    }
    return self;
}

-(void) dealloc
{
    self.rewardedVideoAd.delegate = nil;
    self.rewardedVideoAd = nil;
}

-(void) loadAdWithSetting:(XMUAdSettings *)xmSettings{
    XMAdSettings *xmAdSettings = [xmSettings settings];
    [self.rewardedVideoAd loadWithSettings:xmAdSettings];
}

- (void)show{
    if (self.rewardedVideoAd.isReady) {
        UIViewController *controller = [XMUUtil unityViewController];
        [self.rewardedVideoAd presentFromViewController:controller];
    } else {
        NSLog(@"XMediate : video ad not ready yet");
    }
}

-(BOOL) isReady{
    return [self.rewardedVideoAd isReady];
}

#pragma marl - XMRewardedVideoAdDelegate

- (void)rewardVideoAdDidLoad:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : RewardedVideo Ad Loaded.");
    if(self.adLoadedCallback){
        self.adLoadedCallback(self.rewardedVideoAdClient);
    } else {
        NSLog(@"XMPlugin RewardedVideo : callback is null");
    }
}

- (void)rewardedVideoAd:(XMRewardedVideoAd *)rewardedVideoAd didFailToLoadWithError:(NSError *)error {
    NSLog(@"XMediate : RewardedVideo Ad failed with error %@.",error);
    if (self.adFailedToLoadCallback) {
        NSString *errorMsg = [NSString
                              stringWithFormat:@"Failed to load ad with error: %@", error.description];
        self.adFailedToLoadCallback(self.rewardedVideoAdClient,
                                    [errorMsg cStringUsingEncoding:NSUTF8StringEncoding]);
    } else{
        NSLog(@"XMPlugin Rewarded Video : callback is null");
    }
}

- (void)rewardedVideoAdWillPresent:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdWillPresent.");
    if(self.willPresentCallback){
        self.willPresentCallback(self.rewardedVideoAdClient);
    } else {
        NSLog(@"XMPlugin RewardedVideo : callback is null");
    }
}

- (void)rewardedVideoAdDidPresent:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdDidPresent.");
    if(self.didPresentCallback){
        self.didPresentCallback(self.rewardedVideoAdClient);
    } else {
        NSLog(@"XMPlugin RewardedVideo : callback is null");
    }
}

- (void)rewardedVideoAdDidStartPlaying:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdDidStartPlaying.");
    if(self.didStartPlayingCallback){
        self.didStartPlayingCallback(self.rewardedVideoAdClient);
    } else {
        NSLog(@"XMPlugin RewardedVideo : callback is null");
    }
}

- (void)rewardedVideoAd:(XMRewardedVideoAd *)rewardedVideoAd didFailToPlayWithError:(NSError *)error {
    NSLog(@"XMediate : rewardedVideoAddidFailToPlay WithError %@",error);
    if (self.didFailedToPlayCallback) {
        NSString *errorMsg = [NSString
                              stringWithFormat:@"Failed to play with error: %@", error.description];
        self.didFailedToPlayCallback(self.rewardedVideoAdClient,
                                    [errorMsg cStringUsingEncoding:NSUTF8StringEncoding]);
    } else{
        NSLog(@"XMPlugin Rewarded Video : callback is null");
    }
}

- (void)rewardedVideoAdWillDismiss:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdWillDismiss");
    if(self.willDismissCallback){
        self.willDismissCallback(self.rewardedVideoAdClient);
    } else {
        NSLog(@"XMPlugin RewardedVideo : callback is null");
    }
}

- (void)rewardedVideoAdDidDismiss:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdDidDismiss");
    if(self.didDismissCallback){
        self.didDismissCallback(self.rewardedVideoAdClient);
    } else {
        NSLog(@"XMPlugin RewardedVideo : callback is null");
    }
}

- (void)rewardedVideoAdDidReceiveTapEvent:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdDidReceiveTapEvent");
    if(self.didReceiveTapEventCallback){
        self.didReceiveTapEventCallback(self.rewardedVideoAdClient);
    } else {
        NSLog(@"XMPlugin RewardedVideo : callback is null");
    }
}

- (void)rewardedVideoAdWillLeaveApplication:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdWillLeaveApplication");
    if(self.willLeaveCallback){
        self.willLeaveCallback(self.rewardedVideoAdClient);
    } else {
        NSLog(@"XMPlugin RewardedVideo : callback is null");
    }
}

- (void)rewardedVideoAd:(XMRewardedVideoAd *)rewardedVideoAd didRewardUserWithReward:(XMRewardedVideoReward *)reward {
    NSLog(@"XMediate : rewardedVideoAd didRewardUserWithReward with reward %@",reward);
    if(self.didReceivedRewardCallback){
        self.didReceivedRewardCallback(self.rewardedVideoAdClient,
                                       [reward.rewardType cStringUsingEncoding:NSUTF8StringEncoding],
                                       reward.rewardAmount.doubleValue);
    } else {
        NSLog(@"XMPlugin RewardedVideo : callback is null");
    }
}

- (void)rewardedVideoAdDidExpire:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdDidExpire");
    if(self.didExpireCallback){
        self.didExpireCallback(self.rewardedVideoAdClient);
    } else {
        NSLog(@"XMPlugin RewardedVideo : callback is null");
    }
}
@end
