//
//  XMUBannerAdView.h
//  XMUnityWrapper
//
//  Created by XMediate on 24/08/17.
//  Copyright © 2017 XMediate. All rights reserved.
//
#import "XMSDK.h"
#import "XMUAdType.h"
#import "XMUAdSettings.h"

#import <Foundation/Foundation.h>

@interface XMUBannerAdView : NSObject

- (instancetype)initWithBannerClientReference:(XMUTypeBannerClientRef *)bannerClient adSize:(XMUAdSize)size andPostion:(XMUAdPosition) position;

@property(nonatomic, assign) XMUTypeBannerClientRef *bannerClient;

@property(nonatomic, strong) XMBannerAdView *bannerView;
@property(nonatomic, assign) XMUBannerDidLoadAdCallback adLoadedCallback;
@property(nonatomic, assign) XMUBannerDidFailToLoadAdWithErrorCallback adFailedToLoadCallback;
@property(nonatomic, assign) XMUBannerWillPresentScreenCallback willPresentCallback;
@property(nonatomic, assign) XMUBannerDidDismissScreenCallback didDismissCallback;
@property(nonatomic, assign) XMUBannerWillLeaveApplicationCallback willLeaveCallback;

- (void)loadWith:(XMUAdSettings *)settings;

@end
