//
//  XMCustomEventSettings.h
//  XMediateSDK
//
//  Created by XMediate on 17/04/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMAdSettings.h"

/// Specifies optional ad setting parameters that are provided by the publisher and are
/// forwarded to custom events for purposes of populating an ad request to a 3rd party ad network.
@interface XMCustomEventSettings : NSObject

/// User's gender set in XMAdSettings. If not specified, returns XMGenderUnknown.
@property(nonatomic, readonly, assign) XMGender gender;

/// User's dob month set in XMAdSettings. If not specified, returns 0.
@property(nonatomic, readonly, assign) NSUInteger dobMonth;

/// User's dob day set in XMAdSettings. If not specified, returns 0.
@property(nonatomic, readonly, assign) NSUInteger dobDay;

/// User's dob year set in XMAdSettings. If not specified, returns 0.
@property(nonatomic, readonly, assign) NSUInteger dobYear;

/// User's marital status set in XMAdSettings. If not specified, returns XMMaritalStatusUnKnown.
@property(nonatomic, readonly, assign) XMMaritalStatus maritalStatus;

/// User's sexual orientation set in XMAdSettings. If not specified, returns XMSexualOrientaionUnKnown.
@property(nonatomic, readonly, assign) XMSexualOrientaion sexualOrientation;

/// User's age set in XMAdSettings.If not specified, returns 0.
@property(nonatomic, readonly, assign) NSUInteger age;

/// User's income set in XMAdSettings.If not specified, returns 0.
@property(nonatomic, readonly, assign) NSUInteger income;

/// User's education set in XMAdSettings.If not specified, returns nil.
@property(nonatomic, readonly, copy, nullable) NSString* education;

/// User's language set in XMAdSettings.If not specified, returns nil.
@property(nonatomic, readonly, copy, nullable) NSString* language;

/// User's latitude set in XMAdSettings.If not specified, returns 0.
@property(nonatomic, readonly, assign) CGFloat latitude;

/// User's longitude set in XMAdSettings.If not specified, returns 0.
@property(nonatomic, readonly, assign) CGFloat longitude;

/// User's area code set in XMAdSettings.If not specified, returns nil.
@property(nonatomic, readonly, copy, nullable) NSString *areaCode;

/// Keywords set in XMAdSettings.If not specified, returns nil.
@property(nonatomic, readonly, copy, nullable) NSString *keywords;

/// The extra parameters set in XMAdSettings.If not specified, returns nil.
@property (nonatomic, readonly, copy,nullable) NSDictionary<NSString*,NSString*> *extraParams;

/// The testing property set in XMAdSettings.If not specified, returns NO.
@property(nonatomic, readonly, assign) BOOL isTesting;

/**
 * When testing the ads you can provide your simulator/device's identifier. This will be forwarded to adnetworks theat need them.
 * Device hash id should be without "-" (Hyphen)
 */
@property(nonatomic, readonly, copy) NSArray<NSString*>* _Nullable testDevices;

@end
