//
//  XMBannerAdSize.h
//  XMediateSDK
//
//  Created by XMediate on 22/03/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 @typedef XMAdSize

 Represents the ad (Banner) size.
 */
typedef struct XMAdSize {
  CGSize size;
} XMAdSize;


/*!
 Represents the banner ad size.
 */
extern const XMAdSize XM_BANNER_SIZE;
extern const XMAdSize XM_MEDIUM_RECT_SIZE;
extern const XMAdSize XM_LEADERBOARD_SIZE;
extern const XMAdSize XM_SKYSCRAPER_SIZE;
extern const XMAdSize XM_WIDE_SKYSCRAPER_SIZE;
