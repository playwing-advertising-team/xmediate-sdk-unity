//
//  XMUAdType.h
//  XMUnityWrapper
//
//  Created by XMediate on 14/09/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#ifndef XMUAdType_h
#define XMUAdType_h


#endif 

typedef NS_ENUM(NSUInteger, XMUAdSize) {
    kXMUBanner = 0,
    kXMULeaderBoardSize = 1,
    kXMUMediumRectangle = 2,
    KXMSkyScrapperSize = 3,
    KXMWideSkyScrapperSize = 4
};

typedef NS_ENUM(NSUInteger, XMUAdPosition) {
    kXMAdPositionTop = 0,
    kXMAdPositionBottom = 1,
    kXMAdPositionTopLeft = 2,
    kXMAdPositionTopRight = 3,
    kXMAdPositionBottomLeft = 4,
    kXMAdPositionBottomRight = 5,
    kXMAdPositionCenter = 6
};

typedef const void *XMUTypeRef;

/// type representing unity clients.
typedef const void *XMUTypeBannerClientRef;
typedef const void *XMUTypeFullScreenAdClientRef;
typedef const void *XMUTypeRewardedVideoAdClientRef;

/// Type representing a XM Ads.
typedef const void *XMUTypeBannerRef;
typedef const void *XMUTypeFullScreenAdRef;
typedef const void *XMUTypeRewardedVideoAdRef;

/// Type representing a AdSettings.
typedef const void *XMUTypeAdSettingsRef;

/// Type representing a NSMutableDictionary of extras.
typedef const void *XMUTypeMutableDictionaryRef;

/// Type representing a XMUAdNetworkExtras.
typedef const void *XMUTypeAdNetworkExtrasRef;

// Banner Ad CallBacks
typedef void (*XMUBannerDidLoadAdCallback)(XMUTypeBannerClientRef *bannerClient);
typedef void (*XMUBannerDidFailToLoadAdWithErrorCallback)(XMUTypeBannerClientRef *bannerClient, const char *error);
typedef void (*XMUBannerWillPresentScreenCallback)(XMUTypeBannerClientRef *bannerClient);
typedef void (*XMUBannerWillDismissScreenCallback)(XMUTypeBannerClientRef *bannerClient);
typedef void (*XMUBannerDidDismissScreenCallback)(XMUTypeBannerClientRef *bannerClient);
typedef void (*XMUBannerWillLeaveApplicationCallback)(XMUTypeBannerClientRef *bannerClient);

/// Callback for Rewarded Video Ad
typedef void (*XMURewardedVideoAdDidLoadAdCallback)(XMUTypeRewardedVideoAdClientRef *rewardedVideoAdClient);
typedef void (*XMURewardedVideoAdDidFailToLoadAdWithErrorCallback)(XMUTypeRewardedVideoAdClientRef *rewardedVideoAdClient, const char *error);
typedef void (*XMURewardedVideoAdWillPresentCallback)(XMUTypeRewardedVideoAdClientRef *rewardedVideoAdClient);
typedef void (*XMURewardedVideoAdDidPresentCallback)(XMUTypeRewardedVideoAdClientRef *rewardedVideoAdClient);
typedef void (*XMURewardedVideoAdDidStartPlayingCallback)(XMUTypeRewardedVideoAdClientRef *rewardedVideoAdClient);
typedef void (*XMURewardedVideoAdDidFailToPlayWithErrorCallback)(XMUTypeRewardedVideoAdClientRef *rewardedVideoAdClient, const char *error);
typedef void (*XMURewardedVideoAdWillDismissCallback)(XMUTypeRewardedVideoAdClientRef *rewardedVideoAdClient);
typedef void (*XMURewardedVideoAdDidDismissCallback)(XMUTypeRewardedVideoAdClientRef *rewardedVideoAdClient);
typedef void (*XMURewardedVideoAdDidReceiveTapEventCallback)(XMUTypeRewardedVideoAdClientRef *rewardedVideoAdClient);
typedef void (*XMURewardedVideoAdWillLeaveApplicationCallback)(XMUTypeRewardedVideoAdClientRef *rewardedVideoAdClient);
typedef void (*XMURewardedVideoAdDidReceivedRewardCallback)(XMUTypeRewardedVideoAdClientRef *rewardedVideoAdClient, const char *rewardType, double rewardAmount);
typedef void (*XMURewardedVideoAdDidExpireCallback)(XMUTypeRewardedVideoAdClientRef *rewardedVideoAdClient);

/// Callback for Fullscreen Ad
typedef void (*XMUFullScreenAdDidLoadAdCallback)(XMUTypeFullScreenAdClientRef *fullScreenAdClient);
typedef void (*XMUFullScreenAdDidFailToLoadAdWithErrorCallback)(XMUTypeFullScreenAdClientRef *fullScreenAdClient, const char *error);
typedef void (*XMUFullScreenAdWillPresentCallback)(XMUTypeFullScreenAdClientRef *fullScreenAdClient);
typedef void (*XMUFullScreenAdDidPresentCallback)(XMUTypeFullScreenAdClientRef *fullScreenAdClient);
typedef void (*XMUFullScreenAdWillDismissCallback)(XMUTypeFullScreenAdClientRef *fullScreenAdClient);
typedef void (*XMUFullScreenAdDidDismissCallback)(XMUTypeFullScreenAdClientRef *fullScreenAdClient);
typedef void (*XMUFullScreenAdDidReceiveTapEventCallback)(XMUTypeFullScreenAdClientRef *fullScreenAdClient);
typedef void (*XMUFullScreenAdWillLeaveApplicationCallback)(XMUTypeFullScreenAdClientRef *fullScreenAdClient);
typedef void (*XMUFullScreenAdDidExpireCallback)(XMUTypeFullScreenAdClientRef *fullScreenAdClient);
typedef void (*XMUFullScreenAdStartPlayingVideoCallback)(XMUTypeFullScreenAdClientRef *fullScreenAdClient);
typedef void (*XMUFullScreenAdDidFailToPlayWithErrorCallback)(XMUTypeFullScreenAdClientRef *fullScreenAdClient, const char *error);
typedef void (*XMUFullScreenAdDidFinishPlayingVideoCallback)(XMUTypeFullScreenAdClientRef *fullScreenAdClient);
