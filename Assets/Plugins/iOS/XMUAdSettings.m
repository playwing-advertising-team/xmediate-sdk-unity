//
//  XMUAdSettings.m
//  XMUnityWrapper
//
//  Created by Vikash Gupta on 20/09/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import "XMUAdSettings.h"
#import "XMSDK.h"
@implementation XMUAdSettings

- (id)init {
    self = [super init];
    if (self) {
        self.xmAdSettings = [XMAdSettings settings];
    }
    return self;
}

- (XMAdSettings *)settings {
    return self.xmAdSettings;
}


-(void) addGender:(XMUGender) gender{
    switch(gender){
        case XMUGenderMale:
            self.xmAdSettings.gender = XMGenderMale;
            break;
        case XMUGenderFemale:
            self.xmAdSettings.gender = XMGenderFemale;
            break;
        case XMUGenderOther:
            self.xmAdSettings.gender = XMUGenderOther;
            break;
        case XMUGenderUnKnown:
        default:
            self.xmAdSettings.gender = XMGenderUnKnown;
            break;
            
    }
}
-(void) addMaritalStatus:(XMUMaritalStatus)maritalStatus{
    switch (maritalStatus) {
        case XMUMaritalStatusUnMarried:
            self.xmAdSettings.maritalStatus = XMMaritalStatusUnMarried;
            break;
        case XMUMaritalStatusMarried:
            self.xmAdSettings.maritalStatus = XMMaritalStatusMarried;
            break;
        case XMUMaritalStatusWidowed:
            self.xmAdSettings.maritalStatus = XMMaritalStatusWidowed;
            break;
        case XMUMaritalStatusDivorced:
            self.xmAdSettings.maritalStatus = XMMaritalStatusDivorced;
            break;
        case XMUMaritalStatusUnKnown:
        default:
            self.xmAdSettings.maritalStatus = XMUMaritalStatusUnKnown;
            break;
    }
    
}
-(void) addSexualOrientation:(XMUSexualOrientaion)sexualOrientation{
    switch (sexualOrientation) {
        case XMUSexualOrientaionStraight:
            self.xmAdSettings.sexualOrientation = XMSexualOrientaionStraight;
            break;
        case XMUSexualOrientaionBiSexual:
            self.xmAdSettings.sexualOrientation = XMSexualOrientaionBiSexual;
            break;
        case XMUSexualOrientaionHomoSexual:
            self.xmAdSettings.sexualOrientation = XMSexualOrientaionHomoSexual;
            break;
        case XMUSexualOrientaionUnKnown:
        default:
            self.xmAdSettings.sexualOrientation = XMSexualOrientaionUnKnown;
            break;
    }
}
-(void) addAge:(NSUInteger)age{
    self.xmAdSettings.age = age;
}
-(void) addIncome:(NSUInteger)income{
    self.xmAdSettings.income = income;
}
-(void) addEducation:(NSString *)education{
    self.xmAdSettings.education = education;
}
-(void) addLanguage:(NSString *)language{
    self.xmAdSettings.language = language;
}
-(void) addAreaCode:(NSString *)areaCode{
    self.xmAdSettings.areaCode = areaCode;
}
-(void) addKeywords:(NSString *)keywords{
    self.xmAdSettings.keywords = keywords;
}

-(void) addExtraParams:(NSDictionary<NSString *,NSString *> *)extraParams{
    self.xmAdSettings.extraParams = extraParams;
}
-(void) addTesting:(BOOL)testing{
    self.xmAdSettings.testing = testing;
}
-(void) addTestDevices:(NSArray<NSString *> *)testDevices{
    self.xmAdSettings.testDevices = testDevices;
}
-(void) addDateOfBirthWithMonth:(NSUInteger)month
                            day:(NSUInteger)day
                           year:(NSUInteger)year{
    [self.xmAdSettings setDateOfBirthWithMonth:month day:day year:year];
}

-(void) addLocationWithLatitude:(CGFloat)latitude
                      longitude:(CGFloat)longitude{
    [self.xmAdSettings setLocationWithLatitude:latitude longitude:longitude];
}

@end
