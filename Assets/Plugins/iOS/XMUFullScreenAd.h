//
//  XMUFullScreenAd.h
//  XMediateSDK
//
//  Created by Fedor Melnichenko on 11/9/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import "XMSDK.h"
#import "XMUAdType.h"
#import "XMUAdSettings.h"

@interface XMUFullScreenAd : NSObject

@property (nonatomic, strong) XMFullScreenAd *fullScreenAd;

@property (nonatomic, assign) XMUTypeFullScreenAdClientRef *fullScreenAdClient;

@property (nonatomic, assign) XMUFullScreenAdDidLoadAdCallback adLoadedCallback;
@property (nonatomic, assign) XMUFullScreenAdDidFailToLoadAdWithErrorCallback adFailedToLoadCallback;

@property (nonatomic, assign) XMUFullScreenAdWillPresentCallback willPresentCallback;
@property (nonatomic, assign) XMUFullScreenAdDidPresentCallback didPresentCallback;

@property (nonatomic, assign) XMUFullScreenAdWillDismissCallback willDismissCallback;
@property (nonatomic, assign) XMUFullScreenAdDidDismissCallback didDismissCallback;

@property (nonatomic, assign) XMUFullScreenAdDidReceiveTapEventCallback didReceiveTapEventCallback;
@property (nonatomic, assign) XMUFullScreenAdWillLeaveApplicationCallback willLeaveApplicationCallback;
@property (nonatomic, assign) XMUFullScreenAdDidExpireCallback didExpireCallback;

@property (nonatomic, assign) XMUFullScreenAdStartPlayingVideoCallback startPlayingVideoCallback;
@property (nonatomic, assign) XMUFullScreenAdDidFailToPlayWithErrorCallback didFailToPlayVideoCallback;
@property (nonatomic, assign) XMUFullScreenAdDidFinishPlayingVideoCallback didFinishPlayingVideoCallback;

@property (nonatomic, readonly, getter=isReady) BOOL ready;

- (instancetype)initWithFullScreenAdClientReference:(XMUTypeFullScreenAdClientRef *)fullScreenAdClient;

- (void)loadAdOfType:(XMFullScreenAdType)type withSettings:(XMUAdSettings *)settings;
- (void)loadAdWithSettings:(XMUAdSettings *)settings;
- (void)show;

@end
