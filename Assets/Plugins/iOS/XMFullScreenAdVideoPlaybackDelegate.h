//
//  XMFullScreenAdVideoPlaybackDelegate.h
//  XMediateSDK
//
//  Created by XMediate on 29/09/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol XMFullScreenAdVideoPlaybackDelegate <NSObject>
/**
 * Tells the delegate that the  fullscreen ad started playing.
 *
 * @param fullScreenAd The  fullscreen ad object.
 */
- (void)fullScreenAdStartPlayingVideo:(XMFullScreenAd *)fullScreenAd;


/**
 * This method is called when a  fullscreen fails to play .
 *
 * @param fullScreenAd The  fullscreen ad object.
 * @param error An error describing why the fullscreen couldn't play.
 */
- (void)fullScreenAd:(XMFullScreenAd *)fullScreenAd didFailToPlayWithError:(NSError *)error;


- (void)fullScreenAdDidFinishPlayingVideo:(XMFullScreenAd *)fullScreenAd;
@end
