//
//  XMFullScreenAd.h
//  XMediateSDK
//
//  Created by XMediate on 29/09/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMFullScreenAdDelegate.h"
#import "XMFullScreenAdVideoPlaybackDelegate.h"
#import "XMAdSettings.h"

typedef NS_ENUM(NSInteger, XMFullScreenAdType) {
    XMFullScreenTypeDefault,
    XMFullScreenTypeInterstitial,
    XMFullScreenTypeVideo
};

@interface XMFullScreenAd : NSObject

-(instancetype) initWithDelegate:(id<XMFullScreenAdDelegate>)delegate;

/**
 * The delegate (`XMFullScreenAdDelegate`) of the full screen ad object.
 */
@property (nonatomic, weak) id<XMFullScreenAdDelegate> delegate;
@property (nonatomic, weak) id<XMFullScreenAdVideoPlaybackDelegate> videoPlaybackDelegate;

/**
 * Indicates if the full screen ad is ready to be presented full screen.
 */
@property(nonatomic, readonly, getter=isReady) BOOL ready;

/**
 * Requests a new ad.
 *
 * If the full screen ad is already loading an ad or one is already presnted, this call will be ignored.
 */
-(void) loadFullScreenAdWithSettings:(XMAdSettings*)adSettings;

-(void) loadFullScreenAdOfType:(XMFullScreenAdType)type WithSettings:(XMAdSettings*)adSettings;

@property XMFullScreenAdType fullScreenAdType;

/**
 * Presents the fullscreen ad modally from the specified view controller.
 *
 * This method will do nothing if the fullscreen ad has not been ready (i.e. the value of its `ready` property is NO).
 *
 * `XMFullScreenAdDelegate` provides optional methods that you may implement to stay
 * informed about when an  fullscreen takes over or leaves the screen.
 *
 * @param controller The view controller that should be used to present the fullscreen ad.
 * This view controller must be added to the application key window before you call this method.
 */
- (void)presentFromViewController:(UIViewController *)controller;
@end
