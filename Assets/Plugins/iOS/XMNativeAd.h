//
//  XMNativeAd.h
//  XMediateSDK
//
//  Created by Tarun Sharma on 07/06/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "XMNativeAdDelegate.h"
#import "XMAdSettings.h"
#import "XMNativeAdEventsDelegate.h"
#import "XMNativeAdViewProtocol.h"
#import "XMNativeAdImage.h"

@interface XMNativeAd : NSObject

@property (strong,nonatomic) NSString *headLineText;

@property (strong,nonatomic) NSString *bodyText;

@property (strong,nonatomic) NSString *callToActionText;

@property (strong,nonatomic) XMNativeAdImage *iconOrLogoImage;

@property (strong,nonatomic) XMNativeAdImage *primaryImage;

@property (strong,nonatomic) NSString *advertizerText;

@property (strong,nonatomic) NSDecimalNumber *rating;

@property (strong,nonatomic) NSString *store;

@property (strong,nonatomic) NSString *price;

@property(nonatomic, weak) id<XMNativeAdEventsDelegate> delegate;

- (void) renderNativeAdWithView:(id<XMNativeAdViewProtocol>)nativeAdView;

@end
