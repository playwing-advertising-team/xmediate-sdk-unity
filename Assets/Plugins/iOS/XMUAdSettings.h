//
//  XMUAdSettings.h
//  XMUnityWrapper
//
//  Created by Vikash Gupta on 20/09/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMSDK.h"

typedef NS_ENUM(NSInteger, XMUGender) {
    XMUGenderUnKnown = 0,
    XMUGenderOther = 1,
    XMUGenderMale = 2,
    XMUGenderFemale = 3
};

typedef NS_ENUM(NSInteger,XMUMaritalStatus) {
    XMUMaritalStatusUnKnown = 0,
    XMUMaritalStatusMarried = 1,    /// Married
    XMUMaritalStatusUnMarried = 2,  /// UnMarried
    XMUMaritalStatusDivorced = 3,   /// Divorced
    XMUMaritalStatusWidowed  = 4     /// Widow
};


typedef NS_ENUM(NSInteger,XMUSexualOrientaion) {
    XMUSexualOrientaionUnKnown = 0,
    XMUSexualOrientaionStraight = 1,    /// Straight
    XMUSexualOrientaionBiSexual = 2,    /// BiSexual
    XMUSexualOrientaionHomoSexual = 3   /// HomoSexual
};

@interface XMUAdSettings : NSObject
@property(nonatomic, strong) XMAdSettings *xmAdSettings;

-(void) addGender:(XMUGender) gender;
-(void) addMaritalStatus:(XMUMaritalStatus)maritalStatus;
-(void) addSexualOrientation:(XMUSexualOrientaion)sexualOrientation;
-(void) addAge:(NSUInteger)age;
-(void) addIncome:(NSUInteger)income;
-(void) addEducation:(NSString *)education;
-(void) addLanguage:(NSString *)language;
-(void) addAreaCode:(NSString *)areaCode;
-(void) addKeywords:(NSString *)keywords;

-(void) addExtraParams:(NSDictionary<NSString *,NSString *> *)extraParams;
-(void) addTesting:(BOOL)testing;
-(void) addTestDevices:(NSArray<NSString *> *)testDevices;
-(void) addDateOfBirthWithMonth:(NSUInteger)month
                            day:(NSUInteger)day
                           year:(NSUInteger)year;

-(void) addLocationWithLatitude:(CGFloat)latitude
                      longitude:(CGFloat)longitude;

- (XMAdSettings *)settings;
@end
