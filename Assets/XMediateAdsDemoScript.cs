﻿using UnityEngine;
using System;
using System.Reflection;
using XMediateAds.Api;
using XMediateAds.Api.AdSettings;

public class XMediateAdsDemoScript : MonoBehaviour
{
    #region GDPR consent dialog strings
    const string keySave = "GDPR_Consent";
    const string constText = "We care about your privacy and dat security.\nWe keep this app free by showing ads.\n" +
                                    "Can we continue to use your data to tailor\nads for you?";
    #endregion

    #region GDPR consent dialog widgets

    bool isShowDialog;
    #endregion

    //Main X-Mediate Ads entities
    private BannerView bannerView;
    private FullscreenAdView fullscreenView;
    private RwdVideoView rwdVideoView;

    //Additional demo data
    private float deltaTime = 0.0f;
    private static string outputMessage = string.Empty;

    public void Start()
    {
        //Manual way to initialize X-Mediate library
        this.InitXMediate();
    }

    public void Update()
    {
        // Calculate simple moving average for time to render screen. 0.1 factor used as smoothing value.
        this.deltaTime += (Time.deltaTime - this.deltaTime) * 0.1f;
    }

    public static string OutputMessage
    {
        set { outputMessage = value; }
    }

    public void OnGUI()
    {
        GUIStyle style = new GUIStyle();

        if (isShowDialog)
        {
            ViewDialogWindow();
            return;
        }

        #region FPS label initialization
        Rect rect = new Rect(0, 0, Screen.width, Screen.height);
        style.alignment = TextAnchor.UpperCenter;
        style.fontSize = (int)(Screen.height * 0.06);
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
        float fps = 1.0f / this.deltaTime;
        string text = string.Format("{0:0.} fps", fps);
        GUI.Label(rect, text, style);
        #endregion

        #region Callback methods output message label initialization
        Rect textOutputRect = new Rect(0, 0.1f * Screen.height, Screen.width, Screen.height);
        style.alignment = TextAnchor.UpperCenter;
        style.fontSize = (int)(Screen.height * 0.03);
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
        GUI.Label(textOutputRect, outputMessage, style);
        #endregion

        #region Basic button style declaration
        GUI.skin.button.fontSize = (int)(0.035f * Screen.width);
        float buttonWidth = 0.8f * Screen.width;
        float buttonHeight = 0.12f * Screen.height;
        float columnOnePosition = 0.1f * Screen.width;
        float columnTwoPosition = 0.55f * Screen.width;
        #endregion

        #region Show Banner button declaration
        Rect requestBannerRect = new Rect(columnOnePosition, 0.215f * Screen.height, buttonWidth, buttonHeight);
        if (GUI.Button(requestBannerRect, "Show\nBanner"))
        {
            this.RequestBanner();
        }
        #endregion

        #region Show Fullscreen button declaration
        Rect requestFullscreenRect = new Rect(columnOnePosition, 0.345f * Screen.height, buttonWidth, buttonHeight);
        if (GUI.Button(requestFullscreenRect, "Show\nFullscreen"))
        {
            this.RequestFullscreen();
        }
        #endregion

        #region Show Rewarded button declaration
        Rect requestRwdVideoRect = new Rect(columnOnePosition, 0.475f * Screen.height, buttonWidth, buttonHeight);
        if (GUI.Button(requestRwdVideoRect, "Show\nRwdVideo"))
        {
            this.RequestRwdVideo();
        }
        #endregion


        #region GDPR UI related flow

        Rect requestGDPR = new Rect(columnOnePosition, 0.610f * Screen.height, buttonWidth, buttonHeight);
        if (GUI.Button(requestGDPR, "Show\nGDPR"))
        {
            OpenDialogWindow();
        }
        #endregion

    }

    #region Manual way to initialize X-Mediate library
    private void InitXMediate()
    {
        // These ad units are configured to serve test ads.
#if UNITY_EDITOR
        string pubId = "1bcb9e20-9670-43fd-9b4e-731225f0e2fd";
        string appId = "f50faae5-947a-48af-a252-8b38166d2e74";
#elif UNITY_ANDROID
        string pubId = "b109048b-779e-403d-b064-986608abd1f6";
		string appId = "7058120a-58cc-43d1-992c-9e56a7f445fc";
		//string pubId = "YOUR-ANDROID-PUBLISHER-ID";
		//string appId = "YOUR-ANDROID-APPLICATION-ID";
#elif UNITY_IOS
		string pubId = "YOUR-IOS-PUBLISHER-ID";
		string appId = "YOUR-IOS-APPLICATION-ID";
#endif

        Debug.Log("init :: pubId:" + pubId + ", appId:" + appId);
        XMediate.init(pubId, appId);
    }
    #endregion

    #region X-Mediate Banner Ads usage methods scope
    private void RequestBanner()
    {
        bannerView = XMediate.createBannerAdView(XmBannerType.BANNER, AdPosition.Bottom);

        bannerView.OnBannerLoaded += HandleBannerLoaded;
        bannerView.OnBannerFailedToLoad += HandleBannerFailedToLoad;
        bannerView.OnBannerExpanded += HandleBannerExpanded;
        bannerView.OnBannerClicked += HandleBannerClicked;
        bannerView.OnBannerCollapsed += HandleBannerCollapsed;
        bannerView.OnLeaveApplication += HandleBannerLeftApplication;

        bannerView.LoadAd(null);
    }
    #endregion

    #region X-Mediate Fullscreen Ads usage methods scope
    private void RequestFullscreen()
    {
        fullscreenView = XMediate.createFullscreenAdView();

        fullscreenView.OnFullscreenAdLoaded += HandleFullscreenLoaded;
        fullscreenView.OnFullscreenAdFailedToLoad += HandleFullscreenFailedToLoad;
        fullscreenView.OnFullscreenAdShown += HandleFullscreenShown;
        fullscreenView.OnFullscreenAdStartedPlaying += HandleFullscreenStartedPlaying;
        fullscreenView.OnFullscreenAdFailedToPlay += HandleFullscreenFailedToPlay;
        fullscreenView.OnFullscreenAdClicked += HandleFullscreenClicked;
        fullscreenView.OnFullscreenAdComplete += HandleFullscreenComplete;
        fullscreenView.OnFullscreenAdDismissed += HandleFullscreenDismissed;
        fullscreenView.OnFullscreenAdLeftApplication += HandleFullscreenLeftApplication;
        fullscreenView.OnFullscreenAdExpiring += HandleFullscreenExpiring;

        fullscreenView.LoadAd(null);
    }

    #endregion

    #region X-Mediate Rewarded Ads usage methods scope
    private void RequestRwdVideo()
    {
        rwdVideoView = XMediate.createRewardedAdView();

        rwdVideoView.OnRwdVideoLoaded += HandleRwdVideoLoaded;
        rwdVideoView.OnRwdVideoFailedToLoad += HandleRwdVideoFailedToLoad;
        rwdVideoView.OnRwdVideoOpened += HandleRwdVideoOpened;
        rwdVideoView.OnRwdVideoStartedPlaying += HandleRwdVideoStartedPlaying;
        rwdVideoView.OnRwdVideoFailedToPlay += HandleRwdVideoFailedToPlay;
        rwdVideoView.OnRwdVideoClicked += HandleRwdVideoClicked;
        rwdVideoView.OnRwdVideoComplete += HandleRwdVideoComplete;
        rwdVideoView.OnRwdVideoClosed += HandleRwdVideoClosed;
        rwdVideoView.OnRwdVideoLeftApplication += HandleRwdVideoLeftApplication;
        rwdVideoView.OnRwdVideoExpiring += HandleRwdVideoExpiring;

        rwdVideoView.LoadAd(CreateDummyAdSettings());
    }

    #endregion

    #region GDPR consent management flow
  
    private void SaveByKey(string data)
    {
        PlayerPrefs.SetString(keySave, data);
    }
    private string GetData()
    {
        return PlayerPrefs.GetString(keySave, "null");
    }
  
    #endregion

    #region XMediate settings setup
    private XmAdSettings CreateDummyAdSettings()
    {
        XmAdSettings adSettings = new XmAdSettings();
        adSettings.SetLocation(10.0, 10.0, 1);

        //if testing mode
        adSettings.SetTesting(true);
        adSettings.SetTestDevices(new String[] { "D4E88AD9429C8C70B0A23E00965D0798" });

        //if you want to set age
        adSettings.SetAge(25);

        //date of birth
        adSettings.SetBirthdate(10, 10, 1990);

        //if you want to set gender
        adSettings.SetGender(XmGender.MALE);

        //if you want to set Martial status
        adSettings.SetMartialStatus(XmMaritalStatus.UNMARRIED);

        //If you want to set sexualOrientation
        adSettings.SetSexualOrientation(XmSexualOrientation.STRAIGHT);

        //if you want to set area code
        adSettings.SetAreaCode("411045");

        //if you want to set Education
        adSettings.SetEducation("B.Tech");

        //income
        adSettings.SetIncome(1000);

        //language
        adSettings.SetLanguage("English");

        //some keywords to define user interests or features or any other params
        adSettings.SetKeyWords("cricket, football, and some other things");

        return adSettings;
    }
    #endregion

    #region Banner callback handlers
    public void HandleBannerLoaded()
    {
        MonoBehaviour.print("HandleBannerLoaded event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleBannerFailedToLoad(string errorCode)
    {
        MonoBehaviour.print(
            "HandleBannerFailedToLoad event received with message: " + errorCode);
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleBannerExpanded()
    {
        MonoBehaviour.print("HandleBannerExpanded event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleBannerClicked()
    {
        MonoBehaviour.print("HandleBannerClicked event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleBannerCollapsed()
    {
        MonoBehaviour.print("HandleBannerCollapsed event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleBannerLeftApplication()
    {
        MonoBehaviour.print("HandleBannerLeftApplication event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }
    #endregion

    #region Fullscreen callback handlers
    public void HandleFullscreenLoaded()
    {
        Debug.Log("HandleFullscreenLoaded");
        MonoBehaviour.print("HandleFullscreenLoaded event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;

        this.fullscreenView.Show();
    }

    public void HandleFullscreenFailedToLoad(string errorCode)
    {
        Debug.Log("HandleFullscreenFailedToLoad");
        MonoBehaviour.print("HandleFullscreenFailedToLoad event received with message: " + errorCode);
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleFullscreenShown()
    {
        Debug.Log("HandleFullscreenShown");
        MonoBehaviour.print("HandleFullscreenShown event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleFullscreenStartedPlaying()
    {
        Debug.Log("HandleFullscreenStartedPlaying");
        MonoBehaviour.print("HandleFullscreenStartedPlaying event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleFullscreenFailedToPlay(string errorCode)
    {
        Debug.Log("HandleFullscreenFailedToPlay");
        MonoBehaviour.print(
            "HandleFullscreenFailedToPlay event received with message: " + errorCode);
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleFullscreenClicked()
    {
        Debug.Log("HandleFullscreenClicked");
        MonoBehaviour.print("HandleFullscreenClicked event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleFullscreenComplete()
    {
        Debug.Log("HandleFullscreenComplete");
        MonoBehaviour.print("HandleFullscreenComplete event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleFullscreenDismissed()
    {
        Debug.Log("HandleFullscreenDismissed");
        MonoBehaviour.print("HandleFullscreenDismissed event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleFullscreenLeftApplication()
    {
        Debug.Log("HandleFullscreenLeftApplication");
        MonoBehaviour.print("HandleFullscreenLeftApplication event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleFullscreenExpiring()
    {
        Debug.Log("HandleFullscreenExpiring");
        MonoBehaviour.print("HandleFullscreenExpiring event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    #endregion

    #region Rwd Video callback handlers
    public void HandleRwdVideoLoaded()
    {
        MonoBehaviour.print("HandleRwdVideoLoaded event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;

        rwdVideoView.Show();
    }

    public void HandleRwdVideoFailedToLoad(string errorCode)
    {
        MonoBehaviour.print("HandleRwdVideoFailedToLoad event received with message: " + errorCode);
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleRwdVideoOpened()
    {
        MonoBehaviour.print("HandleRwdVideoOpened event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleRwdVideoStartedPlaying()
    {
        MonoBehaviour.print("HandleRwdVideoStartedPlaying event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleRwdVideoFailedToPlay(string errorCode)
    {
        MonoBehaviour.print("HandleRwdVideoFailedToPlay event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleRwdVideoClicked()
    {
        MonoBehaviour.print("HandleRwdVideoClicked event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleRwdVideoComplete()
    {
        MonoBehaviour.print("HandleRwdVideoComplete event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleRwdVideoClosed()
    {
        MonoBehaviour.print("HandleRwdVideoClosed event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleRwdVideoLeftApplication()
    {
        MonoBehaviour.print("HandleRwdVideoLeftApplication event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }

    public void HandleRwdVideoExpiring()
    {
        MonoBehaviour.print("HandleRwdVideoExpiring event received");
        outputMessage = MethodBase.GetCurrentMethod().Name;
    }
    #endregion

    #region GDPR flow 
    public void CloseDialogWindow()
    {
        isShowDialog = false;
    }
    public void OpenDialogWindow()
    {
        isShowDialog = true;
    }
    void ViewDialogWindow()
    {
        Rect posWindowDialog = new Rect(0.1f*Screen.width, 0.35f * Screen.height , Screen.width*0.8f, Screen.height * 0.35f);
        Rect posTextView = new Rect(0.2f * Screen.width, 0.4f * Screen.height, Screen.width * 0.6f, Screen.height * 0.1f);
        Rect posButtonYesView = new Rect(0.25f * Screen.width, 0.6f * Screen.height, Screen.width * 0.2f, Screen.height * 0.05f);
        Rect posButtonNoView = new Rect(0.55f * Screen.width, 0.6f * Screen.height, Screen.width * 0.2f, Screen.height * 0.05f);

        GUI.contentColor = Color.white;
        GUIStyle style = new GUIStyle();
        style.fontSize = (int)(Screen.height * 0.02);
        style.alignment = TextAnchor.UpperCenter;
        style.fontStyle = FontStyle.Bold;
        style.normal.textColor = Color.white;


        GUI.Box(posWindowDialog, "");
        
        GUI.TextArea(posTextView,constText,style);

        if (GUI.Button(posButtonYesView, "Yes"))
        {
            CloseDialogWindow();
            SaveByKey("Yes");
            XMediate.updateGDPRSettings(true, true);
            //event

        }
        if (GUI.Button(posButtonNoView, "No"))
        {
            CloseDialogWindow();
            SaveByKey("No");
            XMediate.updateGDPRSettings(false, false);
            //event

        }


    }
    #endregion
    
}