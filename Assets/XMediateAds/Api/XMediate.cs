﻿using System;
using UnityEngine;

namespace XMediateAds.Api
{
	public class XMediate
    {
        public static void init(String pubId, String appId)
		{
			XMediateAdsClientFactory.InitXMediate (pubId, appId);
		}

        public static void updateGDPRSettings(bool isGDPRCountry, bool wasGDPRAccepted)
        {
            XMediateAdsClientFactory.UpdateGDPRSettings(isGDPRCountry, wasGDPRAccepted);
        }

        public static BannerView createBannerAdView(XmBannerType xmBannerType, AdPosition adPosition)
        {
            BannerView bannerView = new GameObject("BannerView").AddComponent<BannerView>();
            bannerView.Initialize(xmBannerType, adPosition);
            return bannerView;

        }

        public static FullscreenAdView createFullscreenAdView()
        {
            FullscreenAdView fullscreenAdView = new GameObject("FullscreenAdView").AddComponent<FullscreenAdView>();
            fullscreenAdView.Initialize();
            return fullscreenAdView;

        }

        public static RwdVideoView createRewardedAdView()
        {
            RwdVideoView rewardedVideo = new GameObject("RwdVideoView").AddComponent<RwdVideoView>();
            rewardedVideo.initialize();

            return rewardedVideo;

        }
    }
}

