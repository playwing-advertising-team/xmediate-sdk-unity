﻿using System;
using XMediateAds.Common;
using XMediateAds.Api.AdSettings;
using System.Reflection;
using UnityEngine;

namespace XMediateAds.Api
{
	public class RwdVideoView : MonoBehaviour
    {
        private IRwdVideoClient client;

        public delegate void OnRwdVideoLoadedDelegate();
        public event OnRwdVideoLoadedDelegate OnRwdVideoLoaded;

        public delegate void OnRwdVideoFailedToLoadDelegate(string errorCode);
        public event OnRwdVideoFailedToLoadDelegate OnRwdVideoFailedToLoad;

        public delegate void OnRwdVideoOpenedDelegate();
        public event OnRwdVideoOpenedDelegate OnRwdVideoOpened;

        public delegate void OnRwdVideoStartedPlayingDelegate();
        public event OnRwdVideoStartedPlayingDelegate OnRwdVideoStartedPlaying;

        public delegate void OnRwdVideoFailedToPlayDelegate(string errorCode);
        public event OnRwdVideoFailedToPlayDelegate OnRwdVideoFailedToPlay;

        public delegate void OnRwdVideoClickedDelegate();
        public event OnRwdVideoClickedDelegate OnRwdVideoClicked;

        public delegate void OnRwdVideoCompleteDelegate();
        public event OnRwdVideoCompleteDelegate OnRwdVideoComplete;

        public delegate void OnRwdVideoClosedDelegate();
        public event OnRwdVideoClosedDelegate OnRwdVideoClosed;

        public delegate void OnRwdVideoLeftApplicationDelegate();
        public event OnRwdVideoLeftApplicationDelegate OnRwdVideoLeftApplication;

        public delegate void OnRwdVideoExpiringDelegate();
        public event OnRwdVideoExpiringDelegate OnRwdVideoExpiring;

        public void Awake()
        {
            DontDestroyOnLoad(this);
        }

        public void initialize()
		{
			Type xMediateAdsClientFactory = Type.GetType ("XMediateAds.XMediateAdsClientFactory,Assembly-CSharp");
			MethodInfo method = xMediateAdsClientFactory.GetMethod ("BuildRwdVideoClient", BindingFlags.Static | BindingFlags.Public);

			client = (IRwdVideoClient)method.Invoke (null, null);
			client.CreateRwdVideoView ();

            ConfigureEvents();
        }

		// Loads Rewarded Video.
		public void LoadAd (XmAdSettings adSettings)
		{
			client.LoadAd (adSettings);
		}

		// Displays the ad.
		public void Show ()
		{
			client.ShowRwdVideoView ();
		}

		// Destroys the ad.
		public void Destroy ()
		{
			client.DestroyRwdVideoView ();
		}

        public void OnRwdVideoLoadedCallback(string message)
        {
            if (OnRwdVideoLoaded != null)
            {
                OnRwdVideoLoaded();
            }
        }

        public void OnRwdVideoFailedToLoadCallback(string errorCode)
        {
            if (OnRwdVideoFailedToLoad != null)
            {
                OnRwdVideoFailedToLoad(errorCode);
            }
        }

        public void OnRwdVideoOpenedCallback(string message)
        {
            if (OnRwdVideoOpened != null)
            {
                OnRwdVideoOpened();
            }
        }

        public void OnRwdVideoStartedPlayingCallback(string message)
        {
            if (OnRwdVideoStartedPlaying != null)
            {
                OnRwdVideoStartedPlaying();
            }
        }

        public void OnRwdVideoFailedToPlayCallback(string errorCode)
        {
            if (OnRwdVideoFailedToPlay != null)
            {
                OnRwdVideoFailedToPlay(errorCode);
            }
        }

        public void OnRwdVideoClickedCallback(string message)
        {
            if (OnRwdVideoClicked != null)
            {
                OnRwdVideoClicked();
            }
        }

        public void OnRwdVideoCompleteCallback(string message)
        {
            if (OnRwdVideoComplete != null)
            {
                OnRwdVideoComplete();
            }
        }

        public void OnRwdVideoClosedCallback(string message)
        {
            if (OnRwdVideoClosed != null)
            {
                OnRwdVideoClosed();
            }
        }

        public void OnRwdVideoLeftApplicationCallback(string message)
        {
            if (OnRwdVideoLeftApplication != null)
            {
                OnRwdVideoLeftApplication();
            }
        }

        public void OnRwdVideoExpiringCallback(string message)
        {
            if (OnRwdVideoExpiring != null)
            {
                OnRwdVideoExpiring();
            }
        }

        private void ConfigureEvents()
        {
            this.client.OnRwdVideoLoaded += (sender, args) => {
                if (this.OnRwdVideoLoaded != null)
                {
                    this.OnRwdVideoLoaded();
                }
            };

            this.client.OnRwdVideoFailedToLoad += (sender, args) => {
                if (this.OnRwdVideoFailedToLoad != null)
                {
                    this.OnRwdVideoFailedToLoad(args.ToString());
                }
            };

            this.client.OnRwdVideoOpened += (sender, args) => {
                if (this.OnRwdVideoOpened != null)
                {
                    this.OnRwdVideoOpened();
                }
            };

            this.client.OnRwdVideoStartedPlaying += (sender, args) => {
                if (this.OnRwdVideoStartedPlaying != null)
                {
                    this.OnRwdVideoStartedPlaying();
                }
            };

            this.client.OnRwdVideoFailedToPlay += (sender, args) => {
                if (this.OnRwdVideoFailedToPlay != null)
                {
                    this.OnRwdVideoFailedToPlay(args.ToString());
                }
            };

            this.client.OnRwdVideoClicked += (sender, args) => {
                if (this.OnRwdVideoClicked != null)
                {
                    this.OnRwdVideoClicked();
                }
            };

            this.client.OnRwdVideoComplete += (sender, args) => {
                if (this.OnRwdVideoComplete != null)
                {
                    this.OnRwdVideoComplete();
                }
            };

            this.client.OnRwdVideoClosed += (sender, args) => {
                if (this.OnRwdVideoClosed != null)
                {
                    this.OnRwdVideoClosed();
                }
            };

            this.client.OnRwdVideoLeftApplication += (sender, args) => {
                if (this.OnRwdVideoLeftApplication != null)
                {
                    this.OnRwdVideoLeftApplication();
                }
            };

            this.client.OnRwdVideoExpiring += (sender, args) => {
                if (this.OnRwdVideoExpiring != null)
                {
                    this.OnRwdVideoExpiring();
                }
            };
        }

    }
}