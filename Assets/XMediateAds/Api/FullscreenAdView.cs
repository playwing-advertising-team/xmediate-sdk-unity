﻿using System;
using XMediateAds.Common;
using XMediateAds.Api.AdSettings;
using System.Reflection;
using UnityEngine;

namespace XMediateAds.Api
{
	public class FullscreenAdView : MonoBehaviour
    {
        private IFullscreenClient client;

        public delegate void OnFullscreenAdLoadedDelegate();
        public event OnFullscreenAdLoadedDelegate OnFullscreenAdLoaded;

        public delegate void OnFullscreenAdFailedToLoadDelegate(string errorCode);
        public event OnFullscreenAdFailedToLoadDelegate OnFullscreenAdFailedToLoad;

        public delegate void OnFullscreenAdShownDelegate();
        public event OnFullscreenAdShownDelegate OnFullscreenAdShown;

        public delegate void OnFullscreenAdStartedPlayingDelegate();
        public event OnFullscreenAdStartedPlayingDelegate OnFullscreenAdStartedPlaying;

        public delegate void OnFullscreenAdFailedToPlayDelegate(string errorCode);
        public event OnFullscreenAdFailedToPlayDelegate OnFullscreenAdFailedToPlay;

        public delegate void OnFullscreenAdClickedDelegate();
        public event OnFullscreenAdClickedDelegate OnFullscreenAdClicked;

        public delegate void OnFullscreenAdCompleteDelegate();
        public event OnFullscreenAdCompleteDelegate OnFullscreenAdComplete;

        public delegate void OnFullscreenAdDismissedDelegate();
        public event OnFullscreenAdDismissedDelegate OnFullscreenAdDismissed;

        public delegate void OnFullscreenAdLeftApplicationDelegate();
        public event OnFullscreenAdLeftApplicationDelegate OnFullscreenAdLeftApplication;

        public delegate void OnFullscreenAdExpiringDelegate();
        public event OnFullscreenAdExpiringDelegate OnFullscreenAdExpiring;

        public void Awake()
        {
            DontDestroyOnLoad(this);
        }

        public void Initialize ()
		{
			Type xMediateAdsClientFactory = Type.GetType ("XMediateAds.XMediateAdsClientFactory,Assembly-CSharp");
			MethodInfo method = xMediateAdsClientFactory.GetMethod ("BuildFullscreenClient", BindingFlags.Static | BindingFlags.Public);

			this.client = (IFullscreenClient)method.Invoke (null, null);
			client.CreateFullscreenAdView ();

			ConfigureEvents ();
		}
	
		// Loads fullscreen.
		public void LoadAd (XmAdSettings adSettings)
		{
			client.LoadAd (adSettings);
		}

		// Displays the ad.
		public void Show ()
		{
			client.ShowFullscreenAdView ();
		}

		// Destroys the ad.
		public void Destroy ()
		{
			client.DestroyFullscreenAdView ();
		}

        public void OnFullscreenAdLoadedCallback(string message)
        {
            if (OnFullscreenAdLoaded != null)
            {
               this.OnFullscreenAdLoaded();
            }
        }

        public void OnFullscreenAdFailedToLoadCallback(string errorCode)
        {
            if (OnFullscreenAdFailedToLoad != null)
            {
                this.OnFullscreenAdFailedToLoad(errorCode);
            }
        }

        public void OnFullscreenAdShownCallback(string message)
        {
            if (OnFullscreenAdShown != null)
            {
                this.OnFullscreenAdShown();
            }
        }

        public void OnFullscreenAdStartedPlayingCallback(string message)
        {
            if (OnFullscreenAdStartedPlaying != null)
            {
                this.OnFullscreenAdStartedPlaying();
            }
        }

        public void OnFullscreenAdFailedToPlayCallback(string errorCode)
        {
            if (OnFullscreenAdFailedToPlay != null)
            {
                this.OnFullscreenAdFailedToPlay(errorCode);
            }
        }

        public void OnFullscreenAdClickedCallback(string message)
        {
            if (OnFullscreenAdClicked != null)
            {
                this.OnFullscreenAdClicked();
            }
        }

        public void OnFullscreenAdCompleteCallback(string message)
        {
            if (OnFullscreenAdComplete != null)
            {
                this.OnFullscreenAdComplete();
            }
        }

        public void OnFullscreenAdDismissedCallback(string message)
        {
            if (OnFullscreenAdDismissed != null)
            {
                this.OnFullscreenAdDismissed();
            }
        }

        public void OnFullscreenAdLeftApplicationCallback(string message)
        {
            if (OnFullscreenAdLeftApplication != null)
            {
                this.OnFullscreenAdLeftApplication();
            }
        }

        public void OnFullscreenAdExpiringCallback(string message)
        {
            if (OnFullscreenAdExpiring != null)
            {
                this.OnFullscreenAdExpiring();
            }
        }

		private void ConfigureEvents()
		{
			this.client.OnFullScreenAdLoaded += (sender, args) => {
				if (this.OnFullscreenAdLoaded != null) {
					this.OnFullscreenAdLoaded ();
				}
			};

			this.client.OnFullScreenAdFailedToLoad += (sender, args) => {
				if (this.OnFullscreenAdFailedToLoad != null) {
					this.OnFullscreenAdFailedToLoad (args.ToString());
				}
			};

			this.client.OnFullScreenAdShown += (sender, args) => {
				if (this.OnFullscreenAdShown != null) {
					this.OnFullscreenAdShown ();
				}
			};

			this.client.OnFullScreenAdDismissed += (sender, args) => {
				if (this.OnFullscreenAdDismissed != null) {
					this.OnFullscreenAdDismissed ();
				}
			};

			this.client.OnFullScreenAdClicked += (sender, args) => {
				if (this.OnFullscreenAdClicked != null) {
					this.OnFullscreenAdClicked ();
				}
			};

			this.client.OnFullScreenAdLeaveApplication += (sender, args) => {
				if (this.OnFullscreenAdLeftApplication != null) {
					this.OnFullscreenAdLeftApplication ();
				}
			};

			this.client.OnFullScreenAdExpiring += (sender, args) => {
				if (this.OnFullscreenAdExpiring != null) {
					this.OnFullscreenAdExpiring ();
				}
			};

			this.client.OnFullScreenAdStartedPlaying += (sender, args) => {
				if (this.OnFullscreenAdStartedPlaying != null) {
					this.OnFullscreenAdStartedPlaying ();
				}
			};

			this.client.OnFullScreenAdFailedToPlay += (sender, args) => {
				if (this.OnFullscreenAdFailedToPlay != null) {
					this.OnFullscreenAdFailedToPlay (args.ToString());
				}
			};

		}
    }
}

