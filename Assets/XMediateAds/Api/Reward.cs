﻿using System;

namespace XMediateAds.Api
{
	// Event that occurs when a user is rewarded by a reward based video ad.
	public class Reward : EventArgs
	{
		public string type { get; set; }
		public int amount { get; set; }
	}
}

