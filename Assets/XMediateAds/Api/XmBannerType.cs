
namespace XMediateAds.Api
{
	public enum XmBannerType
	{
		BANNER,
		LARGE_BANNER,
		MEDIUM_RECTANGLE,
		FULL_BANNER,
		LEADER_BOARD
	}
}
