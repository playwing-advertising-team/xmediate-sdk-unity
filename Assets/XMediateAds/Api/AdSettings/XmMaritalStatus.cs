﻿using System;

namespace XMediateAds.Api.AdSettings
{
	public enum XmMaritalStatus
	{
		UNKNOWN,
		MARRIED,
		UNMARRIED,
		DIVORCED,
		WIDOWED
	}
}

