
using System;
using System.Collections.Generic;
using XMediateAds.Api.AdSettings;
using UnityEngine;

namespace XMediateAds.Api.AdSettings
{
	public class XmAdSettings
	{
		private bool mTesting;
		private String[] testDevices;
		private XmLocation location;
		private Dictionary<string, string> targetingParams = new Dictionary<string, string> ();


		public XmAdSettings ()
		{
		}

		public void SetTesting (bool testing)
		{
			mTesting = testing;
		}

		public bool IsTesting ()
		{
			return mTesting;
		}

		public void SetTestDevices (String[] testDevices)
		{
			this.testDevices = testDevices;
		}

		public String[] GetDevices ()
		{
			if (testDevices == null) {
				return new String[0];
			}
			return testDevices;
		}

		public void SetLocation (Double longitude, Double latitude, double accuracy)
		{
			this.location = new XmLocation (longitude, latitude, accuracy);
		}

		public XmLocation GetLocation ()
		{
			return location;
		}

		public Dictionary<string, string> GetTargetingParams(){
			return targetingParams;
		}

		public void SetAge (int age)
		{
			this.targetingParams.Add (XmTargetingParams.AGE, age.ToString ());
		}

		public void SetGender (XmGender gender)
		{
			this.targetingParams.Add (XmTargetingParams.GENDER, gender.ToString ());
		}

		public void SetBirthdate (int month, int day, int year)
		{
			this.targetingParams.Add (XmTargetingParams.DOB, month + "-" + day + "-" + year);
		}

		public void SetAreaCode (String areaCode)
		{
			this.targetingParams.Add (XmTargetingParams.AREA_CODE, areaCode);
		}

		public void SetIncome (int income)
		{
			this.targetingParams.Add (XmTargetingParams.INCOME, income.ToString ());
		}

		public void SetEducation (string education)
		{
			this.targetingParams.Add (XmTargetingParams.EDUCATION, education);
		}

		public void SetMartialStatus (XmMaritalStatus martialStatus)
		{
			this.targetingParams.Add (XmTargetingParams.MARITAL_STATUS, martialStatus.ToString ());
		}

		public void SetLanguage (string language)
		{
			this.targetingParams.Add (XmTargetingParams.LANGUAGE, language);
		}

		public void SetSexualOrientation (XmSexualOrientation sexualOrientation)
		{
			this.targetingParams.Add (XmTargetingParams.SEXUAL_ORIENTATION, sexualOrientation.ToString ());
		}

		public void SetKeyWords (String keyWords)
		{
			this.targetingParams.Add (XmTargetingParams.KEYWORDS, keyWords);
		}
	}
}
