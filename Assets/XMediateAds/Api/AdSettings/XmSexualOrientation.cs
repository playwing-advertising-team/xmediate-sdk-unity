﻿using System;

namespace XMediateAds.Api.AdSettings
{
	public enum XmSexualOrientation
	{
		UNKNOWN,
		STRAIGHT,
		BISEXUAL,
		HOMOSEXUAL
	}
}

