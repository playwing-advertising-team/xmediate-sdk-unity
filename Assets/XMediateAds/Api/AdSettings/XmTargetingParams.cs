﻿using System;

namespace XMediateAds.Api.AdSettings
{
	public class XmTargetingParams
	{
		public static String REQUEST_AGENT = "x-mediate";
		public static String AGE = "age";
		public static String GENDER = "gender";
		public static String DOB = "dob";
		public static String INCOME = "income";
		public static String EDUCATION = "education";
		public static String AREA_CODE = "area_code";
		public static String LANGUAGE = "language";
		public static String MARITAL_STATUS = "marital_status";
		public static String SEXUAL_ORIENTATION = "sexual_orientation";
		public static String KEYWORDS = "keywords";
		public static String EXTRA_PARAMS = "extra_params";
	}
}