﻿using System;

namespace XMediateAds.Api.AdSettings
{
	public class XmLocation
	{
		public Double longitude;
        public Double latitude;
        public Double accuracy;

		XmLocation(Double longitude, Double latitude)
        {
			this.longitude = longitude;
			this.latitude = latitude;
		}

		public XmLocation(Double longitude, Double latitude, double accuracy)
        {
            this.latitude = latitude;
			this.longitude = longitude;
			this.accuracy = accuracy;
		}
	}
}

