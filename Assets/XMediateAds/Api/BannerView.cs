﻿using UnityEngine;
using XMediateAds.Common;
using System;
using System.Reflection;
using XMediateAds.Api.AdSettings;

namespace XMediateAds.Api
{
    public class BannerView : MonoBehaviour
    {
        private IBannerClient client;

        public delegate void OnBannerLoadedDelegate();
        public event OnBannerLoadedDelegate OnBannerLoaded;

        public delegate void OnBannerFailedToLoadDelegate(string errorCode);
        public event OnBannerFailedToLoadDelegate OnBannerFailedToLoad;

        public delegate void OnBannerExpandedDelegate();
        public event OnBannerExpandedDelegate OnBannerExpanded;

        public delegate void OnBannerClickedDelegate();
        public event OnBannerClickedDelegate OnBannerClicked;

        public delegate void OnBannerCollapsedDelegate();
        public event OnBannerCollapsedDelegate OnBannerCollapsed;

        public delegate void OnLeaveApplicationDelegate();
        public event OnLeaveApplicationDelegate OnLeaveApplication;

        public void Awake()
        {
            DontDestroyOnLoad(this);
        }

        public void Initialize(XmBannerType bannerType, AdPosition position)
        {
            Type xMediateAdsClientFactory = Type.GetType("XMediateAds.XMediateAdsClientFactory,Assembly-CSharp");
            MethodInfo method = xMediateAdsClientFactory.GetMethod("BuildBannerClient", BindingFlags.Static | BindingFlags.Public);
            client = (IBannerClient)method.Invoke(null, null);

            client.CreateBannerView(bannerType, position);

            ConfigureEvents();
        }

        public void LoadAd(XmAdSettings adSettings)
        {
            client.LoadAd(adSettings);
        }

        public void Destroy()
        {
            client.DestroyBannerView();
        }

        public void OnBannerLoadedCallback(string message)
        {
            if (OnBannerLoaded != null)
            {
                this.OnBannerLoaded();
            }
        }

        public void OnBannerFailedToLoadCallback(string errorCode)
        {
            if (OnBannerFailedToLoad != null)
            {
                this.OnBannerFailedToLoad(errorCode);
            }
        }

        public void OnBannerExpandedCallback(string message)
        {
            if (OnBannerExpanded != null)
            {
                this.OnBannerExpanded();
            }
        }

        public void OnBannerClickedCallback(string message)
        {
            if (OnBannerClicked != null)
            {
                this.OnBannerClicked();
            }
        }

        public void OnBannerCollapsedCallback(string message)
        {
            if (OnBannerCollapsed != null)
            {
                this.OnBannerCollapsed();
            }
        }

        public void OnLeaveApplicationCallback(string message)
        {
            if (OnLeaveApplication != null)
            {
                this.OnLeaveApplication();
            }
        }

        private void ConfigureEvents()
        {
            this.client.OnBannerLoaded += (sender, args) =>
            {
                if (this.OnBannerLoaded != null)
                {
                    this.OnBannerLoaded();
                }
            };

            this.client.OnBannerFailedToLoad += (sender, args) =>
            {
                if (this.OnBannerFailedToLoad != null)
                {
                    this.OnBannerFailedToLoad(args.ToString());
                }
            };

            this.client.OnBannerExpanded += (sender, args) =>
            {
                if (this.OnBannerExpanded != null)
                {
                    this.OnBannerExpanded();
                }
            };

            this.client.OnBannerClicked += (sender, args) =>
            {
                if (this.OnBannerClicked != null)
                {
                    this.OnBannerClicked();
                }
            };

            this.client.OnBannerCollapsed += (sender, args) =>
            {
                if (this.OnBannerCollapsed != null)
                {
                    this.OnBannerCollapsed();
                }
            };


            this.client.OnLeaveApplication += (sender, args) =>
            {
                if (this.OnLeaveApplication != null)
                {
                    this.OnLeaveApplication();
                }
            };
        }
    }
}