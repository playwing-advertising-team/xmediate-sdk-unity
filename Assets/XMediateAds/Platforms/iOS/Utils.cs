﻿#if UNITY_IOS

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XMediateAds.Api;
using XMediateAds.Api.AdSettings;

namespace XMediateAds.iOS
{
	internal class Utils
	{
		public static int getBannerType(XmBannerType type){
			if (type.Equals (XmBannerType.BANNER)) {
				return 0;
			} else if (type.Equals (XmBannerType.LEADER_BOARD)) {
				return 1;
			} else if (type.Equals (XmBannerType.MEDIUM_RECTANGLE)) {
				return 2;
			} else {
				return 0;
			}
		}

		public static IntPtr BuildXMAdSettings(XmAdSettings adSettings)
		{
			IntPtr settingPtr = XMBridge.XMUCreateAdSetting ();
			if (adSettings == null) {
				return IntPtr.Zero;
			}

			//if testing mode
			XMBridge.XMUSetTesting(settingPtr,adSettings.IsTesting());

			//set location (longitude, latitude)
			if (adSettings.GetLocation () != null) {
				XMBridge.XMUSetLocation (settingPtr, adSettings.GetLocation ().latitude, adSettings.GetLocation ().longitude);
			}

			// get Targeting params
			Dictionary<string, string> targetingParams = adSettings.GetTargetingParams ();
			if (targetingParams != null) {

				//if you want to set age
				if (targetingParams.ContainsKey (XmTargetingParams.AGE)) {
					int age = 0;
					int.TryParse(targetingParams [XmTargetingParams.AGE], out age);
					if (age > 0) {
						XMBridge.XMUSetAge (settingPtr, age);
					}
				}

				//date of birth
				if (targetingParams.ContainsKey (XmTargetingParams.DOB)) {
					string dob = targetingParams [XmTargetingParams.DOB];
					string[] dobArray = dob.Split ('-');
					int month = 0;
					int.TryParse(dobArray [0], out month);
					int day = 0;
					int.TryParse(dobArray [1], out day);
					int year = 0;
					int.TryParse(dobArray [2], out year);
					if (month > 0 && day > 0 && year > 0) {
						XMBridge.XMUSetBirthday (settingPtr, year, month, day);
					}
				}

				//if you want to set gender
				if (targetingParams.ContainsKey (XmTargetingParams.GENDER)) {
					string gender = targetingParams[XmTargetingParams.GENDER];
					int g = 0;
					if (gender.Equals (XmGender.MALE)) {
						g = 2;
					} else if (gender.Equals (XmGender.FEMALE)) {
						g = 3;
					} else if (gender.Equals (XmGender.OTHER)) {
						g = 1;
					} else {
						g = 0;
					}
						
					XMBridge.XMUSetGender(settingPtr, g);
				}

				//if you want to set Martial status
				if (targetingParams.ContainsKey (XmTargetingParams.MARITAL_STATUS)) {
					string maritalStatus = targetingParams[XmTargetingParams.MARITAL_STATUS];
					int marStatus = 0;
					if (maritalStatus.Equals (XmMaritalStatus.MARRIED)) {
						marStatus = 1;
					} else if (maritalStatus.Equals (XmMaritalStatus.UNMARRIED)) {
						marStatus = 2;
					}if (maritalStatus.Equals (XmMaritalStatus.DIVORCED)) {
						marStatus = 3;
					}if (maritalStatus.Equals (XmMaritalStatus.WIDOWED)) {
						marStatus = 4;
					} else {
						marStatus = 0;
					}
					XMBridge.XMUSetMaritalStatus (settingPtr, marStatus);
				}

				//If you want to set sexualOrientation
				if (targetingParams.ContainsKey (XmTargetingParams.SEXUAL_ORIENTATION)) {
					string sexualOrientation = targetingParams[XmTargetingParams.SEXUAL_ORIENTATION];
					int or = 0;
					if (sexualOrientation.Equals (XmSexualOrientation.STRAIGHT)) {
						or = 1;
					} else if (sexualOrientation.Equals (XmSexualOrientation.BISEXUAL)) {
						or = 2;
					} else if (sexualOrientation.Equals (XmSexualOrientation.HOMOSEXUAL)) {
						or = 3;
					} else {
						or = 0;
					}
					XMBridge.XMUSetSexualOrientation (settingPtr,or);
				}

				if (targetingParams.ContainsKey (XmTargetingParams.AREA_CODE)) {
					XMBridge.XMUSetAreaCode (settingPtr, targetingParams [XmTargetingParams.AREA_CODE]);
				}

				//if you want to set Education
				if (targetingParams.ContainsKey (XmTargetingParams.EDUCATION)) {
					XMBridge.XMUSetEducation (settingPtr, targetingParams [XmTargetingParams.EDUCATION]);
				}

				//income
				if (targetingParams.ContainsKey (XmTargetingParams.INCOME)) {
					int income = 0;
					int.TryParse(targetingParams [XmTargetingParams.INCOME], out income);
					if (income > 0) {
						XMBridge.XMUSetIncome (settingPtr, income);
					}
				}

				//language
				if (targetingParams.ContainsKey (XmTargetingParams.LANGUAGE)) {
					XMBridge.XMUSetLanguage (settingPtr, targetingParams [XmTargetingParams.LANGUAGE]);
				}

				//some keywords to define user interests or features or any other params
				if (targetingParams.ContainsKey (XmTargetingParams.KEYWORDS)) {
					XMBridge.XMUSetKeywords (settingPtr, targetingParams [XmTargetingParams.KEYWORDS]);
				}
					
			}
			return settingPtr;
		}


		
	}

}

#endif
