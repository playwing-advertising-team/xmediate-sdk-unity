﻿#if UNITY_IOS

using System;
using UnityEngine;
using System.Reflection;

namespace XMediateAds.iOS
{
	public class XMediate
	{
		public XMediate()
		{
			Debug.Log("iOS " + MethodBase.GetCurrentMethod().Name);
		}

		public static void Init(string pubId, string appId)
		{
			Debug.Log("iOS " + MethodBase.GetCurrentMethod().Name);
			XMBridge.XMUInitialize (pubId, appId);
		}

        public static void UpdateGDPRSettings(bool isGDPRCountry, bool wasGDPRAccepted)
        {
            Debug.Log("iOS " + MethodBase.GetCurrentMethod().Name);
            XMBridge.XMUUpdateGDPRSettings(isGDPRCountry, wasGDPRAccepted);
        }
	}
}

#endif
