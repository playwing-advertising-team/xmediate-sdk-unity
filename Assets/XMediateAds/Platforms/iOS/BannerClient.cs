﻿
#if UNITY_IOS

using AOT;
using System;
using System.Reflection;
using System.Runtime.InteropServices;
using UnityEngine;
using XMediateAds.Common;
using XMediateAds.Api;
using XMediateAds.Api.AdSettings;

namespace XMediateAds.iOS
{
	public class BannerClient  : IBannerClient
	{
		private IntPtr bannerViewPtr;
		private IntPtr bannerClientPtr;

		public BannerClient() {
			Debug.Log("iOS " + MethodBase.GetCurrentMethod().Name);
		}

		#region Banner callback types

		internal delegate void XMUBannerDidLoadAdCallback(IntPtr bannerClient);
		internal delegate void XMUBannerDidFailToLoadAdWithErrorCallback(IntPtr bannerClient, string error);
		internal delegate void XMUBannerWillPresentScreenCallback(IntPtr bannerClient);
		internal delegate void XMUBannerDidDismissScreenCallback(IntPtr bannerClient);
		internal delegate void XMUBannerWillLeaveApplicationCallback(IntPtr bannerClient);

		#endregion

		#region IBannerClient implementation

		public event EventHandler<AdFailedToLoadEventArgs> OnBannerFailedToLoad;
		public event EventHandler<EventArgs> OnBannerLoaded;
		public event EventHandler<EventArgs> OnBannerExpanded;
		public event EventHandler<EventArgs> OnBannerClicked;
		public event EventHandler<EventArgs> OnBannerCollapsed;
		public event EventHandler<EventArgs> OnLeaveApplication;

		// Creates a banner view and adds it to the view hierarchy

		public void CreateBannerView(XmBannerType bannerType, AdPosition position) {
			Debug.Log("iOS - BannerClient : " + MethodBase.GetCurrentMethod().Name);

			this.bannerClientPtr = (IntPtr)GCHandle.Alloc(this);

			int type = Utils.getBannerType(bannerType);
			this.bannerViewPtr = XMBridge.XMUCreateBannerViewWithSizeAndPosition (bannerClientPtr, type, (int)position);

			XMBridge.XMUSetBannerCallbacks(
				this.bannerViewPtr,
				AdViewDidReceiveAdCallback,
				AdViewDidFailToReceiveAdWithErrorCallback,
				AdViewWillPresentScreenCallback,
				AdViewDidDismissScreenCallback,
				AdViewWillLeaveApplicationCallback);
		}

		public void CreateBannerView(XmBannerType bannerType, int x, int y) {
			Debug.Log("iOS - BannerClient : " + MethodBase.GetCurrentMethod().Name);

			this.bannerClientPtr = (IntPtr)GCHandle.Alloc(this);

			int type  = Utils.getBannerType(bannerType);
			this.bannerViewPtr = XMBridge.XMUCreateBannerViewWithSize (bannerClientPtr,type);

			XMBridge.XMUSetBannerCallbacks(
				this.bannerViewPtr,
				AdViewDidReceiveAdCallback,
				AdViewDidFailToReceiveAdWithErrorCallback,
				AdViewWillPresentScreenCallback,
				AdViewDidDismissScreenCallback,
				AdViewWillLeaveApplicationCallback);
		}

		// Requests a new ad for the banner view.
		public void LoadAd(XmAdSettings adSettings) {
			Debug.Log("iOS - BannerClient :" + MethodBase.GetCurrentMethod().Name);

			IntPtr settingPtr = Utils.BuildXMAdSettings (adSettings);
			XMBridge.XMULoadBannerAd (this.bannerViewPtr,settingPtr);
			XMBridge.XMURemoveAdSetting (settingPtr);
		}

		// Destroys a banner view.
		public void DestroyBannerView() {
			Debug.Log("iOS - BannerClient : " + MethodBase.GetCurrentMethod().Name);

			if (bannerViewPtr != IntPtr.Zero) {
				XMBridge.XMURemoveBannerView (bannerViewPtr);
				bannerViewPtr = IntPtr.Zero;
			}
		}

		#endregion

		#region Banner callback methods

		[MonoPInvokeCallback(typeof(XMUBannerDidLoadAdCallback))]
		private static void AdViewDidReceiveAdCallback(IntPtr bannerClient) {
			Debug.Log("iOS - BannerClient : " + MethodBase.GetCurrentMethod().Name+ " execution started");

			BannerClient client = IntPtrToBannerClient(bannerClient);
			if (client.OnBannerLoaded != null) {
				client.OnBannerLoaded(client, EventArgs.Empty);
			}

			Debug.Log("iOS - BannerClient : " + MethodBase.GetCurrentMethod().Name +" execution ended");

		}

		[MonoPInvokeCallback(typeof(XMUBannerDidFailToLoadAdWithErrorCallback))]
		private static void AdViewDidFailToReceiveAdWithErrorCallback(IntPtr bannerClient, string error) {
			Debug.Log("iOS - BannerClient : " + MethodBase.GetCurrentMethod().Name);

			BannerClient client = IntPtrToBannerClient(bannerClient);

			if (client.OnBannerFailedToLoad != null) {
				AdFailedToLoadEventArgs args = new AdFailedToLoadEventArgs() {
					Message = error
				};

				client.OnBannerFailedToLoad(client, args);
			}
		}

		[MonoPInvokeCallback(typeof(XMUBannerWillPresentScreenCallback))]
		private static void AdViewWillPresentScreenCallback(IntPtr bannerClient) {
			Debug.Log("iOS - BannerClient : " + MethodBase.GetCurrentMethod().Name);

			BannerClient client = IntPtrToBannerClient(bannerClient);

			if (client.OnBannerExpanded != null) {
				client.OnBannerExpanded(client, EventArgs.Empty);
			}
		}

		[MonoPInvokeCallback(typeof(XMUBannerDidDismissScreenCallback))]
		private static void AdViewDidDismissScreenCallback(IntPtr bannerClient) {
			Debug.Log("iOS - BannerClient : " + MethodBase.GetCurrentMethod().Name);

			BannerClient client = IntPtrToBannerClient(bannerClient);

			if (client.OnBannerCollapsed != null) {
				client.OnBannerCollapsed(client, EventArgs.Empty);
			}
		}

		[MonoPInvokeCallback(typeof(XMUBannerWillLeaveApplicationCallback))]
		private static void AdViewWillLeaveApplicationCallback(IntPtr bannerClient) {
			Debug.Log("iOS - BannerClient : " + MethodBase.GetCurrentMethod().Name);

			BannerClient client = IntPtrToBannerClient(bannerClient);

			if (client.OnLeaveApplication != null) {
				client.OnLeaveApplication(client, EventArgs.Empty);
			}
		}

		private static BannerClient IntPtrToBannerClient(IntPtr bannerClient) {
			GCHandle handle = (GCHandle)bannerClient;

			return handle.Target as BannerClient;
		}

		#endregion
	}
}
#endif