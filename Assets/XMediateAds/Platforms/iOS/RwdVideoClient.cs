﻿#if UNITY_IOS

using AOT;
using System;
using System.Reflection;
using System.Runtime.InteropServices;
using UnityEngine;
using XMediateAds.Common;
using XMediateAds.Api.AdSettings;
using XMediateAds.Api;

namespace XMediateAds.iOS
{
	public class RwdVideoClient : IRwdVideoClient
	{
		public RwdVideoClient ()
		{
		}

		private IntPtr rwVideoPtr;
		private IntPtr rwVideoClientPtr;

		#region Interstitial callback types

		internal delegate void XMURewardedVideoAdDidLoadAdCallback (IntPtr rwVideoClient);

		internal delegate void XMURewardedVideoAdDidFailToLoadAdWithErrorCallback (IntPtr rwVideoClient,
			string error);

		internal delegate void XMURewardedVideoAdWillPresentCallback (IntPtr rwVideoClient);

		internal delegate void XMURewardedVideoAdDidPresentCallback (IntPtr rwVideoClient);

		internal delegate void XMURewardedVideoAdWillDismissCallback (IntPtr rwVideoClient);

		internal delegate void XMURewardedVideoAdDidDismissCallback (IntPtr rwVideoClient);

		internal delegate void XMURewardedVideoAdDidStartPlayingCallback (IntPtr rwVideoClient);

		internal delegate void XMURewardedVideoAdDidFailToPlayWithErrorCallback (IntPtr rwVideoClient, string error);

		internal delegate void XMURewardedVideoAdDidReceivedRewardCallback (IntPtr rwVideoClient, string rewardType, double rewardAmount);

		internal delegate void XMURewardedVideoAdDidExpireCallback (IntPtr rwVideoClient);

		internal delegate void XMURewardedVideoAdDidReceiveTapEventCallback (IntPtr rwVideoClient);

		internal delegate void XMURewardedVideoAdWillLeaveApplicationCallback (IntPtr rwVideoClient);

		#endregion

		#region IRwdVideoClient implementation

		#pragma warning disable 67

		public event EventHandler<EventArgs> OnRwdVideoLoaded;
		public event EventHandler<AdFailedToLoadEventArgs> OnRwdVideoFailedToLoad;
		public event EventHandler<EventArgs> OnRwdVideoOpened;
		public event EventHandler<EventArgs> OnRwdVideoStartedPlaying;
		public event EventHandler<AdFailedToLoadEventArgs> OnRwdVideoFailedToPlay;
		public event EventHandler<EventArgs> OnRwdVideoClicked;
		public event EventHandler<EventArgs> OnRwdVideoComplete;
		public event EventHandler<EventArgs> OnRwdVideoClosed;
		public event EventHandler<EventArgs> OnRwdVideoLeftApplication;
		public event EventHandler<EventArgs> OnRwdVideoExpiring;

		#pragma warning restore 67

		public void CreateRwdVideoView ()
		{
			Debug.Log ("iOS - rwvideoclient : " + MethodBase.GetCurrentMethod ().Name);
			this.rwVideoClientPtr = (IntPtr)GCHandle.Alloc (this);
			this.rwVideoPtr = XMBridge.XMUCreateRewardedVideoAd (this.rwVideoClientPtr);
			XMBridge.XMUSetRewardedVideoAdCallbacks (
				this.rwVideoPtr,
				RewardedVideoAdDidLoadAdCallback,
				RewardedVideoAdDidFailToLoadAdWithErrorCallback,
				RewardedVideoAdWillPresentCallback,
				RewardedVideoAdDidPresentCallback,
				RewardedVideoAdWillDismissCallback, 
				RewardedVideoAdDidDismissCallback,
				RewardedVideoAdDidStartPlayingCallback,
				RewardedVideoAdDidFailToPlayWithErrorCallback,
				RewardedVideoAdDidExpireCallback,
				RewardedVideoAdDidReceiveTapEventCallback,
				RewardedVideoAdWillLeaveApplicationCallback,
				RewardedVideoAdDidReceivedRewardCallback
			);	
		}

		public void LoadAd (XmAdSettings adSettings)
		{
			Debug.Log ("iOS - rwvideoclient : " + MethodBase.GetCurrentMethod ().Name);
			IntPtr settingPtr = Utils.BuildXMAdSettings (adSettings);
			XMBridge.XMULoadRewardedVideoAd (this.rwVideoPtr,settingPtr);
			XMBridge.XMURemoveAdSetting (settingPtr);
		}

		public void ShowRwdVideoView ()
		{
			Debug.Log ("iOS - rwvideoclient : " + MethodBase.GetCurrentMethod ().Name);
			XMBridge.XMUShowRewardedVideoAd (this.rwVideoPtr);
		}

		public void DestroyRwdVideoView ()
		{
			Debug.Log ("iOS - rwvideoclient : " + MethodBase.GetCurrentMethod ().Name);

			if (rwVideoPtr != IntPtr.Zero) {
				XMBridge.XMURemoveRewardedVideoAd (this.rwVideoPtr);
				this.rwVideoPtr = IntPtr.Zero;
			}
		}

		#endregion


		#region rewarded Video Callbacks

		[MonoPInvokeCallback (typeof(XMURewardedVideoAdDidLoadAdCallback))]
		private static void RewardedVideoAdDidLoadAdCallback (IntPtr rwVideoClient)
		{
			Debug.Log ("iOS - rwvideoclient : " + MethodBase.GetCurrentMethod ().Name);

			RwdVideoClient client = IntPtrToRwVideoClient (rwVideoClient);
			if (client.OnRwdVideoLoaded != null) {
				client.OnRwdVideoLoaded (client, EventArgs.Empty);
			}
		}

		[MonoPInvokeCallback (typeof(XMURewardedVideoAdDidFailToLoadAdWithErrorCallback))]
		private static void RewardedVideoAdDidFailToLoadAdWithErrorCallback (IntPtr rwVideoClient, string error)
		{
			Debug.Log ("iOS - rwdvideoclient : " + MethodBase.GetCurrentMethod ().Name);

			RwdVideoClient client = IntPtrToRwVideoClient (rwVideoClient);
			if (client.OnRwdVideoFailedToLoad != null) {
				AdFailedToLoadEventArgs args = new AdFailedToLoadEventArgs () {
					Message = error
				};
				client.OnRwdVideoFailedToLoad (client, args);
			}
		}

		[MonoPInvokeCallback (typeof(XMURewardedVideoAdWillPresentCallback))]
		private static void RewardedVideoAdWillPresentCallback (IntPtr rwVideoClient)
		{
			Debug.Log ("iOS - rwvideoclient : " + MethodBase.GetCurrentMethod ().Name);

			RwdVideoClient client = IntPtrToRwVideoClient (rwVideoClient);
			if (client.OnRwdVideoOpened != null) {
				client.OnRwdVideoOpened (client, EventArgs.Empty);
			}
		}

		[MonoPInvokeCallback (typeof(XMURewardedVideoAdDidPresentCallback))]
		private static void RewardedVideoAdDidPresentCallback (IntPtr rwVideoClient)
		{
			Debug.Log ("iOS - rwvideoclient : " + MethodBase.GetCurrentMethod ().Name);


		}


		[MonoPInvokeCallback (typeof(XMURewardedVideoAdWillDismissCallback))]
		private static void RewardedVideoAdWillDismissCallback (IntPtr rwVideoClient)
		{
			Debug.Log ("iOS - rwvideoclient : " + MethodBase.GetCurrentMethod ().Name);


		}

		[MonoPInvokeCallback (typeof(XMURewardedVideoAdDidDismissCallback))]
		private static void RewardedVideoAdDidDismissCallback (IntPtr rwVideoClient)
		{
			Debug.Log ("iOS - rwvideoclient : " + MethodBase.GetCurrentMethod ().Name);

			RwdVideoClient client = IntPtrToRwVideoClient (rwVideoClient);
			if (client.OnRwdVideoClosed != null) {
				client.OnRwdVideoClosed (client, EventArgs.Empty);
			}
		}

		[MonoPInvokeCallback (typeof(XMURewardedVideoAdDidStartPlayingCallback))]
		private static void RewardedVideoAdDidStartPlayingCallback (IntPtr rwVideoClient)
		{
			Debug.Log ("iOS - rwvideoclient : " + MethodBase.GetCurrentMethod ().Name);

			RwdVideoClient client = IntPtrToRwVideoClient (rwVideoClient);
			if (client.OnRwdVideoStartedPlaying != null) {
				client.OnRwdVideoStartedPlaying (client, EventArgs.Empty);
			}
		}

		[MonoPInvokeCallback (typeof(XMURewardedVideoAdDidFailToPlayWithErrorCallback))]
		private static void RewardedVideoAdDidFailToPlayWithErrorCallback (IntPtr rwVideoClient, string error)
		{
			Debug.Log ("iOS - rwvideoclient : " + MethodBase.GetCurrentMethod ().Name);

			RwdVideoClient client = IntPtrToRwVideoClient (rwVideoClient);
			if (client.OnRwdVideoFailedToPlay != null) {
				AdFailedToLoadEventArgs args = new AdFailedToLoadEventArgs () {
					Message = error
				};
				client.OnRwdVideoFailedToPlay (client, args);
			}
		}

		[MonoPInvokeCallback (typeof(XMURewardedVideoAdDidReceivedRewardCallback))]
		private static void RewardedVideoAdDidReceivedRewardCallback (IntPtr rwVideoClient, string rewardType, double rewardAmount)
		{
			Debug.Log ("iOS - rwvideoclient : " + MethodBase.GetCurrentMethod ().Name);
			Debug.Log ("iOS - rwvideoclient : Reward[Type :" + rewardType + ", Amount: " + rewardAmount + "]");
		
			RwdVideoClient client = IntPtrToRwVideoClient (rwVideoClient);
			Reward args = new Reward () {
				type = rewardType,
				amount = (int)rewardAmount
			};

			if (client.OnRwdVideoComplete != null) {
				client.OnRwdVideoComplete (client, args);
			}
		}


		[MonoPInvokeCallback (typeof(XMURewardedVideoAdDidExpireCallback))]
		private static void RewardedVideoAdDidExpireCallback (IntPtr rwVideoClient)
		{
			Debug.Log ("iOS - rwvideoclient : " + MethodBase.GetCurrentMethod ().Name);

			RwdVideoClient client = IntPtrToRwVideoClient (rwVideoClient);
			if (client.OnRwdVideoExpiring != null) {
				client.OnRwdVideoExpiring (client, EventArgs.Empty);
			}
		}

		[MonoPInvokeCallback (typeof(XMURewardedVideoAdDidReceiveTapEventCallback))]
		private static void RewardedVideoAdDidReceiveTapEventCallback (IntPtr rwVideoClient)
		{
			Debug.Log ("iOS - rwvideoclient : " + MethodBase.GetCurrentMethod ().Name);

			RwdVideoClient client = IntPtrToRwVideoClient (rwVideoClient);
			if (client.OnRwdVideoClicked != null) {
				client.OnRwdVideoClicked (client, EventArgs.Empty);
			}
		}

		[MonoPInvokeCallback (typeof(XMURewardedVideoAdWillLeaveApplicationCallback))]
		private static void RewardedVideoAdWillLeaveApplicationCallback (IntPtr rwVideoClient)
		{
			Debug.Log ("iOS - rwvideoclient : " + MethodBase.GetCurrentMethod ().Name);
			RwdVideoClient client = IntPtrToRwVideoClient (rwVideoClient);
			if (client.OnRwdVideoLeftApplication != null) {
				client.OnRwdVideoLeftApplication (client, EventArgs.Empty);
			}
		}


		private static RwdVideoClient IntPtrToRwVideoClient (IntPtr rwVideoClient)
		{
			GCHandle handle = (GCHandle)rwVideoClient;
			return handle.Target as RwdVideoClient;
		}

		#endregion
	}
}

#endif