﻿#if UNITY_IOS

using System;
using System.Runtime.InteropServices;

namespace XMediateAds.iOS
{
	internal class XMBridge
	{
#region Common externs

		[DllImport ("__Internal")]
		internal static extern void XMUInitialize (string pubId, string appId);

        [DllImport ("__Internal")]
		internal static extern void XMUUpdateGDPRSettings(bool isGDPRCountry, bool wasGDPRAccepted);

#endregion

#region Banner Externs

		[DllImport ("__Internal")]
		internal static extern IntPtr XMUCreateBannerViewWithSizeAndPosition (IntPtr bannerClient, int size, int pos);

		[DllImport ("__Internal")]
		internal static extern IntPtr XMUCreateBannerViewWithSize (IntPtr bannerClient, int size);

		[DllImport ("__Internal")]
		internal static extern void XMURemoveBannerView (IntPtr bannerViewPtr);

		[DllImport ("__Internal")]
		internal static extern void XMUSetBannerCallbacks (
			IntPtr bannerView,
			BannerClient.XMUBannerDidLoadAdCallback adReceivedCallback,
			BannerClient.XMUBannerDidFailToLoadAdWithErrorCallback adFailedCallback,
			BannerClient.XMUBannerWillPresentScreenCallback willPresentCallback,
			BannerClient.XMUBannerDidDismissScreenCallback didDismissCallback,
			BannerClient.XMUBannerWillLeaveApplicationCallback willLeaveCallback
		);

		[DllImport ("__Internal")]
		internal static extern void XMULoadBannerAd (IntPtr bannerView, IntPtr adSettings);

#endregion

#region Fullscreen Externs

		[DllImport("__Internal")]
		internal static extern IntPtr XMUCreateFullScreenAd(IntPtr fullscreenClient);

		[DllImport("__Internal")]
		internal static extern void XMURemoveFullScreenAd(IntPtr fullscreenPtr);

		[DllImport("__Internal")]
		internal static extern void XMUSetFullScreenAdCallbacks(IntPtr fullscreenAd,
								FullscreenClient.XMUFullScreenAdDidLoadAdCallback adLoadedCallback,
								FullscreenClient.XMUFullScreenAdDidFailToLoadAdWithErrorCallback adFailedToLoadCallback,
								FullscreenClient.XMUFullScreenAdWillPresentCallback willPresentCallback,
								FullscreenClient.XMUFullScreenAdDidPresentCallback didPresentCallback,
								FullscreenClient.XMUFullScreenAdWillDismissCallback willDismissCallback,
								FullscreenClient.XMUFullScreenAdDidDismissCallback didDismissCallback,
								FullscreenClient.XMUFullScreenAdDidReceiveTapEventCallback didReceiveTapEventCallback,
								FullscreenClient.XMUFullScreenAdWillLeaveApplicationCallback willLeaveApplicationCallback,
								FullscreenClient.XMUFullScreenAdDidExpireCallback didExpireCallback,
								FullscreenClient.XMUFullScreenAdStartPlayingVideoCallback startPlayingVideoCallback,
								FullscreenClient.XMUFullScreenAdDidFailToPlayWithErrorCallback didFailToPlayVideoCallback,
								FullscreenClient.XMUFullScreenAdDidFinishPlayingVideoCallback didFinishPlayingVideoCallback);

		[DllImport("__Internal")]
		internal static extern void XMUShowFullScreenAd(IntPtr fullscreenAd);

		[DllImport("__Internal")]
		internal static extern void XMULoadFullScreenAd(IntPtr fullscreenAd, IntPtr adSettings);

#endregion

#region Rewarded Video Externs

		[DllImport ("__Internal")]
		internal static extern IntPtr XMUCreateRewardedVideoAd (IntPtr rwVideoClient);

		[DllImport ("__Internal")]
		internal static extern void XMURemoveRewardedVideoAd (IntPtr rwVideoPtr);

		[DllImport ("__Internal")]
		internal static extern void XMUSetRewardedVideoAdCallbacks (
			IntPtr rwVideo,
			RwdVideoClient.XMURewardedVideoAdDidLoadAdCallback adLoadedCallback,
			RwdVideoClient.XMURewardedVideoAdDidFailToLoadAdWithErrorCallback adFailedCallback,
			RwdVideoClient.XMURewardedVideoAdWillPresentCallback willPresentCallback,
			RwdVideoClient.XMURewardedVideoAdDidPresentCallback didPresentCallback,
			RwdVideoClient.XMURewardedVideoAdWillDismissCallback willDismissCallback,
			RwdVideoClient.XMURewardedVideoAdDidDismissCallback didDismissCallback,
			RwdVideoClient.XMURewardedVideoAdDidStartPlayingCallback didStartPlayingCallback,
			RwdVideoClient.XMURewardedVideoAdDidFailToPlayWithErrorCallback didFailedToPlayCallback,
			RwdVideoClient.XMURewardedVideoAdDidExpireCallback didExpireCallback,
			RwdVideoClient.XMURewardedVideoAdDidReceiveTapEventCallback didReceiveTapEventCallback,
			RwdVideoClient.XMURewardedVideoAdWillLeaveApplicationCallback willLeaveCallback,
			RwdVideoClient.XMURewardedVideoAdDidReceivedRewardCallback didReceiveRewardCallback
		);

		[DllImport ("__Internal")]
		internal static extern void XMULoadRewardedVideoAd (IntPtr rwVideo, IntPtr adSettings);

		[DllImport ("__Internal")]
		internal static extern void XMUShowRewardedVideoAd (IntPtr rwVideo);
#endregion

		[DllImport ("__Internal")]
		internal static extern IntPtr XMUCreateAdSetting ();

		[DllImport ("__Internal")]
		internal static extern void XMURemoveAdSetting (IntPtr adSettingRef);

		[DllImport ("__Internal")]
		internal static extern void XMUSetGender (IntPtr settings, int gender);

		[DllImport ("__Internal")]
		internal static extern void XMUSetMaritalStatus (IntPtr settings, int maritalStatus);

		[DllImport ("__Internal")]
		internal static extern void XMUSetSexualOrientation (IntPtr settings, int sexualOrientation);

		[DllImport ("__Internal")]
		internal static extern void XMUSetAge (IntPtr settings, int age);

		[DllImport ("__Internal")]
		internal static extern void XMUSetIncome (IntPtr settings, int income);

		[DllImport ("__Internal")]
		internal static extern void XMUSetKeywords (IntPtr settings, string keywords);

		[DllImport ("__Internal")]
		internal static extern void XMUSetEducation (IntPtr settings, string eduction);

		[DllImport ("__Internal")]
		internal static extern void XMUSetLanguage (IntPtr settings, string language);

		[DllImport ("__Internal")]
		internal static extern void XMUSetAreaCode (IntPtr settings, string areacode);

		[DllImport ("__Internal")]
		internal static extern void XMUSetTesting (IntPtr settings, bool isTesting);

		[DllImport ("__Internal")]
		internal static extern void XMUSetBirthday (IntPtr settings, int year, int month, int day);

		[DllImport ("__Internal")]
		internal static extern void XMUSetLocation (IntPtr settings, double latitude, double longitude);
	}
}

#endif
