﻿#if UNITY_IOS

using AOT;
using System;
using System.Reflection;
using System.Runtime.InteropServices;
using UnityEngine;
using XMediateAds.Common;
using XMediateAds.Api.AdSettings;
using XMediateAds.Api;

namespace XMediateAds.iOS
{
	public class FullscreenClient : IFullscreenClient
	{
		private IntPtr fullscreenPtr;
		private IntPtr fullscreenClientPtr;

		public FullscreenClient() {}

		#region Fullscreen callback types

		internal delegate void XMUFullScreenAdDidLoadAdCallback(IntPtr fullscreenClient); 									 
		internal delegate void XMUFullScreenAdDidFailToLoadAdWithErrorCallback(IntPtr fullscreenClient, string error);		
		internal delegate void XMUFullScreenAdWillPresentCallback(IntPtr fullscreenClient);									
		internal delegate void XMUFullScreenAdDidPresentCallback(IntPtr fullscreenClient);									
		internal delegate void XMUFullScreenAdWillDismissCallback(IntPtr fullscreenClient);									 
		internal delegate void XMUFullScreenAdDidDismissCallback(IntPtr fullscreenClient);									
		internal delegate void XMUFullScreenAdDidReceiveTapEventCallback(IntPtr fullscreenClient);							
		internal delegate void XMUFullScreenAdWillLeaveApplicationCallback(IntPtr fullscreenClient);						
		internal delegate void XMUFullScreenAdDidExpireCallback(IntPtr fullscreenClient);									
		internal delegate void XMUFullScreenAdStartPlayingVideoCallback(IntPtr fullscreenClient);							
		internal delegate void XMUFullScreenAdDidFailToPlayWithErrorCallback(IntPtr fullscreenClient, string error);		
		internal delegate void XMUFullScreenAdDidFinishPlayingVideoCallback(IntPtr fullscreenClient);						

		#endregion

		#region IFullscreenClient implementation

		#pragma warning disable 67

		public event EventHandler<EventArgs> OnFullScreenAdLoaded;
		public event EventHandler<AdFailedToLoadEventArgs> OnFullScreenAdFailedToLoad;
		public event EventHandler<EventArgs> OnFullScreenAdShown;
		public event EventHandler<EventArgs> OnFullScreenAdStartedPlaying;
		public event EventHandler<AdFailedToLoadEventArgs> OnFullScreenAdFailedToPlay;
		public event EventHandler<EventArgs> OnFullScreenAdClicked;
		public event EventHandler<EventArgs> OnFullScreenAdComplete;
		public event EventHandler<EventArgs> OnFullScreenAdDismissed;
		public event EventHandler<EventArgs> OnFullScreenAdLeaveApplication;
		public event EventHandler<EventArgs> OnFullScreenAdExpiring;

		#pragma warning restore 67

		public void CreateFullscreenAdView()
		{	
			Debug.Log ("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);

			this.fullscreenClientPtr = (IntPtr)GCHandle.Alloc(this);
			this.fullscreenPtr = XMBridge.XMUCreateFullScreenAd(this.fullscreenClientPtr);

			XMBridge.XMUSetFullScreenAdCallbacks(
				this.fullscreenPtr,
				FullscreenAdDidLoadAdCallback,
				FullscreenAdDidFailToLoadAdWithErrorCallback,
				FullscreenAdWillPresentCallback,
				FullscreenAdDidPresentCallback,
				FullscreenAdWillDismissCallback,
				FullscreenAdDidDismissCallback,
				FullscreenAdDidReceiveTapEventCallback,
				FullscreenAdWillLeaveApplicationCallback,
				FullScreenAdDidExpireCallback,
				FullscreenAdStartPlayingVideoCallback,
				FullscreenAdDidFailToPlayWithErrorCallback,
				FullscreenAdDidFinishPlayingVideoCallback
			);
		}

		public void LoadAd (XmAdSettings adSettings)
		{
			Debug.Log ("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);

			IntPtr settingsPtr = Utils.BuildXMAdSettings(adSettings);
			XMBridge.XMULoadFullScreenAd(this.fullscreenPtr, settingsPtr);
			XMBridge.XMURemoveAdSetting (settingsPtr);
		}

		public void ShowFullscreenAdView ()
		{
			Debug.Log ("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);
			XMBridge.XMUShowFullScreenAd(this.fullscreenPtr);
		}

		public void DestroyFullscreenAdView ()
		{
			Debug.Log ("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);

			if (fullscreenPtr != IntPtr.Zero) {
				XMBridge.XMURemoveFullScreenAd (this.fullscreenPtr);
				this.fullscreenPtr = IntPtr.Zero;
			}
		}

		#endregion


		#region Fullscreen Callbacks

		[MonoPInvokeCallback(typeof(XMUFullScreenAdDidLoadAdCallback))]
		private static void FullscreenAdDidLoadAdCallback(IntPtr fullscreenClient) {
			Debug.Log ("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);

			FullscreenClient client = IntPtr2FullscreenClient(fullscreenClient);

			if (client.OnFullScreenAdLoaded != null) {
				client.OnFullScreenAdLoaded (client, EventArgs.Empty);
			}
		}

		[MonoPInvokeCallback(typeof(XMUFullScreenAdDidFailToLoadAdWithErrorCallback))]
		private static void FullscreenAdDidFailToLoadAdWithErrorCallback(IntPtr fullscreenClient, string error) {
			Debug.Log ("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);

			FullscreenClient client = IntPtr2FullscreenClient(fullscreenClient);
			if (client.OnFullScreenAdFailedToLoad != null) {
				AdFailedToLoadEventArgs args = new AdFailedToLoadEventArgs () {
					Message = error
				};
				client.OnFullScreenAdFailedToLoad(client, args);
			}
		}

		[MonoPInvokeCallback(typeof(XMUFullScreenAdWillPresentCallback))]
		private static void FullscreenAdWillPresentCallback(IntPtr fullscreenClient) {
			Debug.Log("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);
		}

		[MonoPInvokeCallback(typeof(XMUFullScreenAdDidPresentCallback))]
		private static void FullscreenAdDidPresentCallback(IntPtr fullscreenClient) {
			Debug.Log("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);

			FullscreenClient client = IntPtr2FullscreenClient(fullscreenClient);
			if (client.OnFullScreenAdShown != null) {
				client.OnFullScreenAdShown(client, EventArgs.Empty);
			}
		}

		[MonoPInvokeCallback(typeof(XMUFullScreenAdWillDismissCallback))]
		private static void FullscreenAdWillDismissCallback(IntPtr fullscreenClient) {
			Debug.Log("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);
		}

		[MonoPInvokeCallback(typeof(XMUFullScreenAdDidDismissCallback))]
		private static void FullscreenAdDidDismissCallback(IntPtr fullscreenClient) {
			Debug.Log("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);

			FullscreenClient client = IntPtr2FullscreenClient(fullscreenClient);
			if (client.OnFullScreenAdDismissed != null) {
				client.OnFullScreenAdDismissed(client, EventArgs.Empty);
			}
		}

		[MonoPInvokeCallback(typeof(XMUFullScreenAdDidReceiveTapEventCallback))] 
		private static void FullscreenAdDidReceiveTapEventCallback(IntPtr fullscreenClient) {
			Debug.Log("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);

			FullscreenClient client = IntPtr2FullscreenClient(fullscreenClient);
			if (client.OnFullScreenAdClicked != null) {
				client.OnFullScreenAdClicked(client, EventArgs.Empty);
			}
		}

		[MonoPInvokeCallback(typeof(XMUFullScreenAdWillLeaveApplicationCallback))]
		private static void FullscreenAdWillLeaveApplicationCallback(IntPtr fullscreenClient) {
			Debug.Log("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);

			FullscreenClient client = IntPtr2FullscreenClient(fullscreenClient);
			if (client.OnFullScreenAdLeaveApplication != null) {
				client.OnFullScreenAdLeaveApplication(client, EventArgs.Empty);
			}
		}

		[MonoPInvokeCallback(typeof(XMUFullScreenAdDidExpireCallback))]
		private static void FullScreenAdDidExpireCallback(IntPtr fullscreenClient) {
			Debug.Log("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);

			FullscreenClient client = IntPtr2FullscreenClient(fullscreenClient);
			if (client.OnFullScreenAdExpiring != null) {
				client.OnFullScreenAdExpiring(client, EventArgs.Empty);
			}
		}

		[MonoPInvokeCallback(typeof(XMUFullScreenAdStartPlayingVideoCallback))]
		private static void FullscreenAdStartPlayingVideoCallback(IntPtr fullscreenClient) {
			Debug.Log("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);

			FullscreenClient client = IntPtr2FullscreenClient(fullscreenClient);
			if (client.OnFullScreenAdStartedPlaying != null) {
				client.OnFullScreenAdStartedPlaying(client, EventArgs.Empty);
			}
		}

		[MonoPInvokeCallback(typeof(XMUFullScreenAdDidFailToPlayWithErrorCallback))]
		private static void FullscreenAdDidFailToPlayWithErrorCallback(IntPtr fullscreenClient, string error) {
			Debug.Log("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);

			FullscreenClient client = IntPtr2FullscreenClient(fullscreenClient);
			if (client.OnFullScreenAdFailedToPlay != null) {
				AdFailedToLoadEventArgs args = new AdFailedToLoadEventArgs() {
					Message = error
				};
				client.OnFullScreenAdFailedToPlay(client, args);
			}
		}

		[MonoPInvokeCallback(typeof(XMUFullScreenAdDidFinishPlayingVideoCallback))]
		private static void FullscreenAdDidFinishPlayingVideoCallback(IntPtr fullscreenClient) {
			Debug.Log("iOS - Fullscreen Client : " + MethodBase.GetCurrentMethod ().Name);

			FullscreenClient client = IntPtr2FullscreenClient(fullscreenClient);
			if (client.OnFullScreenAdComplete != null) {
				client.OnFullScreenAdComplete(client, EventArgs.Empty);
			}
		}

		#endregion

		private static FullscreenClient IntPtr2FullscreenClient(IntPtr fullscreenClient) {
			GCHandle handle = (GCHandle)fullscreenClient;
			return handle.Target as FullscreenClient;
		}
	}
}

#endif