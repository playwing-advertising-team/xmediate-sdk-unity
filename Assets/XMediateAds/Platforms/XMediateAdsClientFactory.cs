using XMediateAds.Common;

namespace XMediateAds
{
	public class XMediateAdsClientFactory
	{
		public static void InitXMediate (string pubId, string appId)
		{
			#if UNITY_ANDROID
				XMediateAds.Android.XMediate.Init (pubId, appId);
			#elif UNITY_IOS
				XMediateAds.iOS.XMediate.Init(pubId, appId);
			#else
				XMediateAds.Common.DummyXMediate.Init(pubId, appId);
			#endif
		}

        public static void UpdateGDPRSettings(bool isGDPRCountry, bool wasGDPRAccepted)
        {
            #if UNITY_ANDROID
                XMediateAds.Android.XMediate.UpdateGDPRSettings(isGDPRCountry, wasGDPRAccepted);
            #elif UNITY_IOS
				XMediateAds.iOS.XMediate.UpdateGDPRSettings(isGDPRCountry, wasGDPRAccepted);
            #else
				XMediateAds.Common.DummyXMediate.UpdateGDPRSettings(isGDPRCountry, wasGDPRAccepted);
            #endif
        }

        public static IBannerClient BuildBannerClient ()
		{
			#if UNITY_ANDROID
				return new XMediateAds.Android.BannerClient ();
			#elif UNITY_IOS
				return new XMediateAds.iOS.BannerClient();
			#else
				return new XMediateAds.Common.DummyClient();
			#endif
		}

		public static IFullscreenClient BuildFullscreenClient ()
		{
			#if UNITY_ANDROID
				return new XMediateAds.Android.FullscreenClient ();
			#elif (UNITY_IOS)
				return new XMediateAds.iOS.FullscreenClient ();
			#else
				return new XMediateAds.Common.DummyClient();
			#endif
		}

		public static IRwdVideoClient BuildRwdVideoClient ()
		{
			#if UNITY_ANDROID
				return new XMediateAds.Android.RwdVideoClient ();
			#elif (UNITY_IOS)
				return new XMediateAds.iOS.RwdVideoClient();
			#else
				return new XMediateAds.Common.DummyClient();
			#endif
		}
	}
}
