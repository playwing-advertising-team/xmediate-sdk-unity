﻿#if UNITY_ANDROID

using System;
using XMediateAds.Common;
using XMediateAds.Api;
using UnityEngine;
using XMediateAds.Api.AdSettings;

namespace XMediateAds.Android
{
	public class FullscreenClient : AndroidJavaProxy, IFullscreenClient
	{
		private AndroidJavaObject fullscreenView;

		public FullscreenClient () : base (Utils.XmuFullscreenListenerClassName)
		{
			var unityPlayer = new AndroidJavaClass (Utils.UnityActivityClassName);
			var activity = unityPlayer.GetStatic<AndroidJavaObject> ("currentActivity");
			this.fullscreenView = new AndroidJavaObject (Utils.XmuFullscreenClassName, activity, this);
		}

		#region IFullscreenClient implementation

		public event EventHandler<EventArgs> OnFullScreenAdLoaded;

		public event EventHandler<AdFailedToLoadEventArgs> OnFullScreenAdFailedToLoad;

		public event EventHandler<EventArgs> OnFullScreenAdShown;

		public event EventHandler<EventArgs> OnFullScreenAdStartedPlaying;

		public event EventHandler<AdFailedToLoadEventArgs> OnFullScreenAdFailedToPlay;

		public event EventHandler<EventArgs> OnFullScreenAdClicked;

		public event EventHandler<EventArgs> OnFullScreenAdComplete;

		public event EventHandler<EventArgs> OnFullScreenAdDismissed;

		public event EventHandler<EventArgs> OnFullScreenAdLeaveApplication;

		public event EventHandler<EventArgs> OnFullScreenAdExpiring;

		public void CreateFullscreenAdView ()
		{
			this.fullscreenView.Call("create");
		}

		public void LoadAd (XmAdSettings adSettings)
		{
			this.fullscreenView.Call("loadAd", Utils.GetAdSettingsJavaObject(adSettings));
		}

		public bool IsLoaded ()
		{
			return this.fullscreenView.Call<bool>("isLoaded");
		}

		public void ShowFullscreenAdView ()
		{
			this.fullscreenView.Call("show");
		}

		public void DestroyFullscreenAdView ()
		{
			this.fullscreenView.Call("destroy");
		}

		#endregion

		#region Callbacks from XmuFullscreenAdListener.

		public void onFullScreenAdLoaded ()
		{
			if (this.OnFullScreenAdLoaded != null) {
				this.OnFullScreenAdLoaded (this, EventArgs.Empty);
			}
		}
			
		public void onFullScreenAdFailedToLoad (string errorReason)
		{
			if (this.OnFullScreenAdFailedToLoad != null) {
				AdFailedToLoadEventArgs args = new AdFailedToLoadEventArgs () {
					Message = errorReason
				};
				this.OnFullScreenAdFailedToLoad (this, args);
			}
		}

		public void onFullScreenAdShown ()
		{
			if (this.OnFullScreenAdShown != null) {
				this.OnFullScreenAdShown (this, EventArgs.Empty);
			}
		}

		public void onFullScreenAdStartedPlaying () 
		{
			if (this.OnFullScreenAdStartedPlaying != null) {
				this.OnFullScreenAdStartedPlaying (this, EventArgs.Empty);
			}
		}

		public void onFullScreenAdFailedToPlay (String errorReason)
		{
			if (this.OnFullScreenAdFailedToPlay != null) {
				AdFailedToLoadEventArgs args = new AdFailedToLoadEventArgs () {
					Message = errorReason
				};
				this.OnFullScreenAdFailedToPlay (this, args);
			}
		}

		public void onFullScreenAdClicked ()
		{
			if (this.OnFullScreenAdClicked != null) {
				this.OnFullScreenAdClicked (this, EventArgs.Empty);
			}
		}

		public void onFullScreenAdComplete ()
		{
			if (this.OnFullScreenAdComplete != null) {
				this.OnFullScreenAdComplete (this, EventArgs.Empty);
			}
		}

		public void onFullScreenAdDismissed ()
		{
			if (this.OnFullScreenAdDismissed != null) {
				this.OnFullScreenAdDismissed (this, EventArgs.Empty);
			}
		}

		public void onFullScreenAdLeaveApplication ()
		{
			if (this.OnFullScreenAdLeaveApplication != null) {
				this.OnFullScreenAdLeaveApplication (this, EventArgs.Empty);
			}
		}

		public void onFullScreenAdExpiring ()
		{
			if (this.OnFullScreenAdExpiring != null) {
				this.OnFullScreenAdExpiring (this, EventArgs.Empty);
			}
		}

		#endregion
	}
}

#endif