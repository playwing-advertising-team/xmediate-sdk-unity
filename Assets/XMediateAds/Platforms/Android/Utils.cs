﻿#if UNITY_ANDROID

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XMediateAds.Api;
using System;
using XMediateAds.Api.AdSettings;

namespace XMediateAds.Android
{
	internal class Utils
	{
		#region Fully-qualified class names

		#region X-Mediate Ads SDK class names

		public const string XMediateClassName = "com.xmediate.base.XMediate";

		public const string XmAdSettingsClassName = "com.xmediate.base.ads.adsettings.XmAdSettings";
		public const string XmGenderClassName = "com.xmediate.base.ads.adsettings.XmGender";
		public const string XmMaritalStatusClassName = "com.xmediate.base.ads.adsettings.XmMaritalStatus";
		public const string XmSexualOrientationClassName = "com.xmediate.base.ads.adsettings.XmSexualOrientation";

		public const string XmBannerTypeClassName = "com.xmediate.base.ads.XmBannerType";

		#endregion

		#region X-Mediate Ads Unity Plugin class names

		public const string XmuBannerClassName = "com.xmediate.unitybridge.XmuBanner";
		public const string XmuBannerAdListenerClassName = "com.xmediate.unitybridge.XmuBannerAdListener";

		public const string XmuFullscreenClassName = "com.xmediate.unitybridge.XmuFullscreenAd";
		public const string XmuFullscreenListenerClassName = "com.xmediate.unitybridge.XmuFullscreenAdListener";

		public const string XmuRwdVideoClassName = "com.xmediate.unitybridge.XmuRwdVideo";
		public const string XmuRwdVideoAdListenerClassName = "com.xmediate.unitybridge.XmuRwdVideoAdListener";

		#endregion

		#region Unity class names

		//public const string UnityActivityClassName = "com.unity3d.player.UnityPlayerActivity";
		 public const string UnityActivityClassName = "com.unity3d.player.UnityPlayer";

		#endregion

		#region Android SDK class names

		public const string BundleClassName = "android.os.Bundle";
		public const string DateClassName = "java.util.Date";

		#endregion

		#endregion

		#region JavaObject creators

		//		public static AndroidJavaObject GetAdSizeJavaObject (XmBannerType bannerType)
		//		{
		//			int width = 320;
		//			int height = 50;
		//			switch (bannerType) {
		//			case XmBannerType.BANNER:
		//				width = 320;
		//				height = 50;
		//				break;
		//			case XmBannerType.LARGE_BANNER:
		//				width = 320;
		//				height = 100;
		//				break;
		//			case XmBannerType.MEDIUM_RECTANGLE:
		//				width = 300;
		//				height = 250;
		//				break;
		//			case XmBannerType.FULL_BANNER:
		//				width = 468;
		//				height = 60;
		//				break;
		//			case XmBannerType.LEADER_BOARD:
		//				width = 728;
		//				height = 90;
		//				break;
		//			}
		//			return new AndroidJavaObject (XmBannerTypeClassName, width, height);
		//		}


		private static AndroidJavaObject javaArrayFromCS (string[] values)
		{
			AndroidJavaClass arrayClass = new AndroidJavaClass ("java.lang.reflect.Array");
			AndroidJavaObject arrayObject = arrayClass.CallStatic<AndroidJavaObject> ("newInstance",
				                                new AndroidJavaClass ("java.lang.String"),
				                                values.Length);
			for (int i = 0; i < values.Length; ++i) {
				arrayClass.CallStatic ("set", arrayObject, i,
					new AndroidJavaObject ("java.lang.String", values [i]));
			}
			return arrayObject;
		}

		public static AndroidJavaObject GetAdSettingsJavaObject (XmAdSettings adSettings)
		{
			if (adSettings == null) {
				return null;
			}

			AndroidJavaObject xmAdSettings = new AndroidJavaObject (XmAdSettingsClassName);
			//if testing mode
			xmAdSettings.Call ("setTesting", adSettings.IsTesting ());
			if (adSettings.IsTesting ()) {
				xmAdSettings.Call ("setTestDevices", javaArrayFromCS (adSettings.GetDevices()));
			}

			//set location (longitude, latitude, and accuracy)
			if (adSettings.GetLocation () != null) {
				xmAdSettings.Call ("setLocation", adSettings.GetLocation ().latitude, adSettings.GetLocation ().longitude, adSettings.GetLocation ().accuracy);
			}

			// get Targeting params
			Dictionary<string, string> targetingParams = adSettings.GetTargetingParams ();
			if (targetingParams != null) {

				//if you want to set age
				if (targetingParams.ContainsKey (XmTargetingParams.AGE)) {
					int age = 0;
					int.TryParse(targetingParams [XmTargetingParams.AGE], out age);
					if (age > 0) {
						xmAdSettings.Call ("setAge", age);
					}
				}

				//date of birth
				if (targetingParams.ContainsKey (XmTargetingParams.DOB)) {
					string dob = targetingParams [XmTargetingParams.DOB];
					string[] dobArray = dob.Split ('-');
					int month = 0;
					int.TryParse(dobArray [0], out month);
					int day = 0;
					int.TryParse(dobArray [1], out day);
					int year = 0;
					int.TryParse(dobArray [2], out year);
					if (month > 0 && day > 0 && year > 0) {
						xmAdSettings.Call ("setDateOfBirth", month, day, year);
					}
				}
					
				//if you want to set gender
				if (targetingParams.ContainsKey (XmTargetingParams.GENDER)) {
					string gender = targetingParams[XmTargetingParams.GENDER];
					AndroidJavaObject xmGender = new AndroidJavaClass(XmGenderClassName).GetStatic<AndroidJavaObject>(gender);
					xmAdSettings.Call ("setGender", xmGender);
				}

				//if you want to set Martial status
				if (targetingParams.ContainsKey (XmTargetingParams.MARITAL_STATUS)) {
					string maritalStatus = targetingParams[XmTargetingParams.MARITAL_STATUS];
					AndroidJavaObject xmMaritalStatus = new AndroidJavaClass(XmMaritalStatusClassName).GetStatic<AndroidJavaObject>(maritalStatus);
					xmAdSettings.Call ("setMartialStatus", xmMaritalStatus);
				}

				//If you want to set sexualOrientation
				if (targetingParams.ContainsKey (XmTargetingParams.SEXUAL_ORIENTATION)) {
					string sexualOrientation = targetingParams[XmTargetingParams.SEXUAL_ORIENTATION];
					AndroidJavaObject xmSexualOrientation = new AndroidJavaClass(XmSexualOrientationClassName).GetStatic<AndroidJavaObject>(sexualOrientation);
					xmAdSettings.Call ("setSexualOrientation", xmSexualOrientation);
				}

				//if you want to set area code
				if (targetingParams.ContainsKey (XmTargetingParams.AREA_CODE)) {
					xmAdSettings.Call ("setAreaCode", targetingParams [XmTargetingParams.AREA_CODE]);
				}

				//if you want to set Education
				if (targetingParams.ContainsKey (XmTargetingParams.EDUCATION)) {
					xmAdSettings.Call ("setEducation", targetingParams [XmTargetingParams.EDUCATION]);
				}

				//income
				if (targetingParams.ContainsKey (XmTargetingParams.INCOME)) {
					int income = 0;
					int.TryParse(targetingParams [XmTargetingParams.INCOME], out income);
					if (income > 0) {
						xmAdSettings.Call ("setIncome", income);
					}
				}

				//language
				if (targetingParams.ContainsKey (XmTargetingParams.LANGUAGE)) {
					xmAdSettings.Call ("setLanguage", targetingParams [XmTargetingParams.LANGUAGE]);
				}

				//some keywords to define user interests or features or any other params
				if (targetingParams.ContainsKey (XmTargetingParams.KEYWORDS)) {
					xmAdSettings.Call ("setKeyWords", targetingParams [XmTargetingParams.KEYWORDS]);
				}

				//			//Any other custom parameter
				//			if (targetingParams.ContainsKey (XmTargetingParams.EXTRA_PARAMS)) {
				//				xmAdSettings.Call ("setExtraParameterSet", targetingParams[XmTargetingParams.EXTRA_PARAMS]);
				//			}
				//
			}
			return xmAdSettings;
		}

		#endregion
	}
}

#endif
