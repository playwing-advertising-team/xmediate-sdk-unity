﻿#if UNITY_ANDROID

using System.Reflection;
using UnityEngine;
using System.Threading;

namespace XMediateAds.Android
{
    public class XMediate
    {
        public XMediate()
        {
            Debug.Log("Android " + MethodBase.GetCurrentMethod().Name);
        }

        public static void Init(string pubId, string appId)
        {
            AndroidJNIHelper.debug = true;
            Debug.Log("Android " + MethodBase.GetCurrentMethod().Name);

            var unityPlayer = new AndroidJavaClass(Utils.UnityActivityClassName);
            Debug.Log("unityPlayer " + unityPlayer);

            Thread.SpinWait(500);

            if (unityPlayer != null)
            {
                var activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                Debug.Log("activity " + activity);

                Thread.SpinWait(500);

                if (activity != null)
                {
                    var application = activity.Call<AndroidJavaObject>("getApplication"); //getApplicationContext
                    Debug.Log("application " + application);

                    var xMediateClass = new AndroidJavaClass(Utils.XMediateClassName);
                    xMediateClass.CallStatic("init", new object[3] { application, pubId, appId });
                }
            }
		}

        public static void UpdateGDPRSettings(bool isGDPRCountry, bool wasGDPRAccepted)
        {
            AndroidJNIHelper.debug = true;
            Debug.Log("Android " + MethodBase.GetCurrentMethod().Name);

            var unityPlayer = new AndroidJavaClass(Utils.UnityActivityClassName);
            Debug.Log("unityPlayer " + unityPlayer);


            if (unityPlayer != null)
            {
                var activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                Debug.Log("activity " + activity);

                if (activity != null)
                {
                    var application = activity.Call<AndroidJavaObject>("getApplication"); //getApplicationContext
                    Debug.Log("application " + application);

                    var xMediateClass = new AndroidJavaClass(Utils.XMediateClassName);
                    xMediateClass.CallStatic("updateGDPRSettings", new object[3] { application, isGDPRCountry, wasGDPRAccepted });
                }
            }
        }
    }
}
#endif