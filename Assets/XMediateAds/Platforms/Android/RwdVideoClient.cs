﻿#if UNITY_ANDROID

using System;
using XMediateAds.Common;
using UnityEngine;
using XMediateAds.Android;
using XMediateAds.Api;
using XMediateAds.Api.AdSettings;

namespace XMediateAds.Android
{
	public class RwdVideoClient : AndroidJavaProxy, IRwdVideoClient
	{
		private AndroidJavaObject rwdVideoView;

		public RwdVideoClient () : base (Utils.XmuRwdVideoAdListenerClassName)
		{
			var unityPlayer = new AndroidJavaClass (Utils.UnityActivityClassName);
			var activity = unityPlayer.GetStatic<AndroidJavaObject> ("currentActivity");
			this.rwdVideoView = new AndroidJavaObject (Utils.XmuRwdVideoClassName, activity, this);
		}

		#region IRwdVideoClient implementation

		public event EventHandler<EventArgs> OnRwdVideoLoaded;

		public event EventHandler<AdFailedToLoadEventArgs> OnRwdVideoFailedToLoad;

		public event EventHandler<EventArgs> OnRwdVideoOpened;

		public event EventHandler<EventArgs> OnRwdVideoStartedPlaying;

		public event EventHandler<AdFailedToLoadEventArgs> OnRwdVideoFailedToPlay;

		public event EventHandler<EventArgs> OnRwdVideoClicked;

		public event EventHandler<EventArgs> OnRwdVideoComplete;

		public event EventHandler<EventArgs> OnRwdVideoClosed;

		public event EventHandler<EventArgs> OnRwdVideoLeftApplication;

		public event EventHandler<EventArgs> OnRwdVideoExpiring;

		public void CreateRwdVideoView ()
		{
			this.rwdVideoView.Call("create");
		}

		public void LoadAd (XmAdSettings adSettings)
		{
			this.rwdVideoView.Call("loadAd", Utils.GetAdSettingsJavaObject(adSettings));
		}

		public bool IsLoaded ()
		{
			return this.rwdVideoView.Call<bool>("isLoaded");
		}

		public void ShowRwdVideoView ()
		{
			this.rwdVideoView.Call("show");
		}

		public void DestroyRwdVideoView ()
		{
			this.rwdVideoView.Call("destroy");
		}
			
		#endregion

		#region Callbacks from XmuRwdVideoAdListener.

		public void onRwdVideoLoaded ()
		{
			if (this.OnRwdVideoLoaded != null) {
				this.OnRwdVideoLoaded (this, EventArgs.Empty);
			}
		}

		public void onRwdVideoFailedToLoad (string errorReason)
		{
			if (this.OnRwdVideoFailedToLoad != null) {
				AdFailedToLoadEventArgs args = new AdFailedToLoadEventArgs () {
					Message = errorReason
				};
				this.OnRwdVideoFailedToLoad (this, args);
			}
		}

		public void onRwdVideoOpened ()
		{
			if (this.OnRwdVideoOpened != null) {
				this.OnRwdVideoOpened (this, EventArgs.Empty);
			}
		}

		public void onRwdVideoStartedPlaying () 
		{
			if (this.OnRwdVideoStartedPlaying != null) {
				this.OnRwdVideoStartedPlaying (this, EventArgs.Empty);
			}
		}

		public void onRwdVideoFailedToPlay (String errorReason)
		{
			if (this.OnRwdVideoFailedToPlay != null) {
				AdFailedToLoadEventArgs args = new AdFailedToLoadEventArgs () {
					Message = errorReason
				};
				this.OnRwdVideoFailedToPlay (this, args);
			}
		}

		public void onRwdVideoClicked ()
		{
			if (this.OnRwdVideoClicked != null) {
				this.OnRwdVideoClicked (this, EventArgs.Empty);
			}
		}

		public void onRwdVideoComplete (String type, int amount)
		{
			if (this.OnRwdVideoComplete != null) {
				Reward args = new Reward()
				{
					type = type,
					amount = amount
				};
				this.OnRwdVideoComplete (this, args);
			}
		}

		public void onRwdVideoClosed ()
		{
			if (this.OnRwdVideoClosed != null) {
				this.OnRwdVideoClosed (this, EventArgs.Empty);
			}
		}

		public void onRwdVideoLeftApplication ()
		{
			if (this.OnRwdVideoLeftApplication != null) {
				this.OnRwdVideoLeftApplication (this, EventArgs.Empty);
			}
		}

		public void onRwdVideoExpiring ()
		{
			if (this.OnRwdVideoExpiring != null) {
				this.OnRwdVideoExpiring (this, EventArgs.Empty);
			}
		}

		#endregion
	}
}
#endif