﻿
#if UNITY_ANDROID

using System;
using System.Reflection;
using XMediateAds.Common;
using XMediateAds.Api;
using UnityEngine;
using XMediateAds.Api.AdSettings;

namespace XMediateAds.Android
{
	public class BannerClient : AndroidJavaProxy, IBannerClient
	{
		private AndroidJavaObject bannerView;

		public BannerClient () : base (Utils.XmuBannerAdListenerClassName)
		{
			var unityPlayer = new AndroidJavaClass (Utils.UnityActivityClassName);
			var activity = unityPlayer.GetStatic<AndroidJavaObject> ("currentActivity");
			this.bannerView = new AndroidJavaObject (Utils.XmuBannerClassName, activity, this);
		}
			
		public event EventHandler<EventArgs> OnBannerLoaded;

		public event EventHandler<AdFailedToLoadEventArgs> OnBannerFailedToLoad;

		public event EventHandler<EventArgs> OnBannerExpanded;

		public event EventHandler<EventArgs> OnBannerClicked;

		public event EventHandler<EventArgs> OnBannerCollapsed;

		public event EventHandler<EventArgs> OnLeaveApplication;
	
		// Creates a banner view.
		public void CreateBannerView (XmBannerType bannerType, AdPosition position)
		{
			this.bannerView.Call (
				"create",
				new object[2] { bannerType.ToString (), (int)position });
		}

		// Creates a banner view with a custom position.
		public void CreateBannerView (XmBannerType bannerType, int x, int y)
		{
			this.bannerView.Call (
				"create",
				new object[3] { bannerType.ToString (), x, y });
		}

		// Loads an ad.
		public void LoadAd (XmAdSettings adSettings)
		{
			this.bannerView.Call ("loadAd", Utils.GetAdSettingsJavaObject (adSettings));
		}

		//		// Displays the banner view on the screen.
		//		public void ShowBannerView()
		//		{
		//			this.bannerView.Call("show");
		//		}

		//		// Hides the banner view from the screen.
		//		public void HideBannerView()
		//		{
		//			this.bannerView.Call("hide");
		//		}
		//
		// Destroys the banner view.
		public void DestroyBannerView ()
		{
			this.bannerView.Call ("destroy");
		}

		#region Callbacks from XmuBannerAdListener.

		public void onBannerLoaded ()
		{
			if (this.OnBannerLoaded != null) {
				this.OnBannerLoaded (this, EventArgs.Empty);
			}
		}

		public void onBannerFailedToLoad (string errorReason)
		{
			if (this.OnBannerFailedToLoad != null) {
				AdFailedToLoadEventArgs args = new AdFailedToLoadEventArgs () {
					Message = errorReason
				};
				this.OnBannerFailedToLoad (this, args);
			}
		}

		public void onBannerExpanded ()
		{
			if (this.OnBannerExpanded != null) {
				this.OnBannerExpanded (this, EventArgs.Empty);
			}
		}

		public void onBannerClicked () 
		{
			if (this.OnBannerClicked != null) {
				this.OnBannerClicked (this, EventArgs.Empty);
			}
		}

		public void onBannerCollapsed ()
		{
			if (this.OnBannerCollapsed != null) {
				this.OnBannerCollapsed (this, EventArgs.Empty);
			}
		}

		public void onLeaveApplication ()
		{
			if (this.OnLeaveApplication != null) {
				this.OnLeaveApplication (this, EventArgs.Empty);
			}
		}

		#endregion
	}
}

#endif

