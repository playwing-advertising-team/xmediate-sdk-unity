﻿using System;
using XMediateAds.Api;
using XMediateAds.Api.AdSettings;

namespace XMediateAds.Common
{
	public interface IFullscreenClient
	{
		event EventHandler<EventArgs> OnFullScreenAdLoaded;
		event EventHandler<AdFailedToLoadEventArgs> OnFullScreenAdFailedToLoad;
		event EventHandler<EventArgs> OnFullScreenAdShown;
		event EventHandler<EventArgs> OnFullScreenAdStartedPlaying;
		event EventHandler<AdFailedToLoadEventArgs> OnFullScreenAdFailedToPlay;
		event EventHandler<EventArgs> OnFullScreenAdClicked;
		event EventHandler<EventArgs> OnFullScreenAdComplete;
		event EventHandler<EventArgs> OnFullScreenAdDismissed;
		event EventHandler<EventArgs> OnFullScreenAdLeaveApplication;
		event EventHandler<EventArgs> OnFullScreenAdExpiring;


		// Creates an Fullscreen ad.
		void CreateFullscreenAdView();

		// Loads a new Fullscreen ad request.
		void LoadAd(XmAdSettings adSettings);

		// Shows the Fullscreen ad.
		void ShowFullscreenAdView();

		// Destroys an Fullscreen ad.
		void DestroyFullscreenAdView();
	}
}