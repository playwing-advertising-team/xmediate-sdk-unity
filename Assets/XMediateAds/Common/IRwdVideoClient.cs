﻿using System;
using XMediateAds.Api;
using XMediateAds.Api.AdSettings;

namespace XMediateAds.Common
{
	public interface IRwdVideoClient
	{
		event EventHandler<EventArgs> OnRwdVideoLoaded;
		event EventHandler<AdFailedToLoadEventArgs> OnRwdVideoFailedToLoad;
		event EventHandler<EventArgs> OnRwdVideoOpened;
		event EventHandler<EventArgs> OnRwdVideoStartedPlaying;
		event EventHandler<AdFailedToLoadEventArgs> OnRwdVideoFailedToPlay;
		event EventHandler<EventArgs> OnRwdVideoClicked;
		event EventHandler<EventArgs> OnRwdVideoComplete;
		event EventHandler<EventArgs> OnRwdVideoClosed;
		event EventHandler<EventArgs> OnRwdVideoLeftApplication;
		event EventHandler<EventArgs> OnRwdVideoExpiring;

		// Creates an Rewarded Video ad.
		void CreateRwdVideoView();

		// Loads a new Rewarded Video request.
		void LoadAd(XmAdSettings adSettings);

		// Shows the Rewarded Video.
		void ShowRwdVideoView();

		// Destroys an Rewarded Video.
		void DestroyRwdVideoView();
	}
}