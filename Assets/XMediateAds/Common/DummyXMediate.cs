﻿using System.Reflection;
using UnityEngine;

namespace XMediateAds.Common
{
	public class DummyXMediate
	{
		public DummyXMediate ()
		{
			Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
		}

		public static void Init(string pubId, string appId)
		{
			Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
		}

        public static void UpdateGDPRSettings(bool isGDPRCountry, bool wasGDPRAccepted)
        {
            Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
        }

    }
}

