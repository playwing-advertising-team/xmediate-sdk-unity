﻿
using System;
using System.Reflection;
using XMediateAds.Api;
using UnityEngine;
using XMediateAds.Api.AdSettings;

namespace XMediateAds.Common
{
	public class DummyClient : IBannerClient, IFullscreenClient, IRwdVideoClient
	{
		public DummyClient() 
		{
			Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
		}

		#pragma warning disable 67

		public event EventHandler<EventArgs> OnBannerLoaded;

		public event EventHandler<AdFailedToLoadEventArgs> OnBannerFailedToLoad;

		public event EventHandler<EventArgs> OnBannerExpanded;

		public event EventHandler<EventArgs> OnBannerClicked;

		public event EventHandler<EventArgs> OnBannerCollapsed;

		public event EventHandler<EventArgs> OnLeaveApplication;
//		-------------------------------------------------------------------------------------
		public event EventHandler<EventArgs> OnFullScreenAdLoaded;

		public event EventHandler<AdFailedToLoadEventArgs> OnFullScreenAdFailedToLoad;

		public event EventHandler<EventArgs> OnFullScreenAdShown;

		public event EventHandler<EventArgs> OnFullScreenAdStartedPlaying;

		public event EventHandler<AdFailedToLoadEventArgs> OnFullScreenAdFailedToPlay;

		public event EventHandler<EventArgs> OnFullScreenAdClicked;

		public event EventHandler<EventArgs> OnFullScreenAdComplete;

		public event EventHandler<EventArgs> OnFullScreenAdDismissed;

		public event EventHandler<EventArgs> OnFullScreenAdLeaveApplication;

		public event EventHandler<EventArgs> OnFullScreenAdExpiring;
//		-------------------------------------------------------------------------------------
		public event EventHandler<EventArgs> OnRwdVideoLoaded;

		public event EventHandler<AdFailedToLoadEventArgs> OnRwdVideoFailedToLoad;

		public event EventHandler<EventArgs> OnRwdVideoOpened;

		public event EventHandler<EventArgs> OnRwdVideoStartedPlaying;

		public event EventHandler<AdFailedToLoadEventArgs> OnRwdVideoFailedToPlay;

		public event EventHandler<EventArgs> OnRwdVideoClicked;

		public event EventHandler<EventArgs> OnRwdVideoComplete;

		public event EventHandler<EventArgs> OnRwdVideoClosed;

		public event EventHandler<EventArgs> OnRwdVideoLeftApplication;

		public event EventHandler<EventArgs> OnRwdVideoExpiring;

		#pragma warning restore 67

		#region IBannerClient implementation

		// Creates a banner view and adds it to the view hierarchy.
		public void CreateBannerView(XmBannerType bannerType, AdPosition position){
			Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
		}

		// Creates a banner view and adds it to the view hierarchy with a custom position.
		public void CreateBannerView(XmBannerType bannerType, int x, int y){
			Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
		}

		// Requests a new ad for the banner view.
		public void LoadAd(XmAdSettings adSettings){
			Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
		}

		// Destroys a banner view.
		public void DestroyBannerView(){
			Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
		}

		// Determines whether the Fullscreen ad has loaded.
		public bool IsLoaded(){
			Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
			return true;
		}

		#endregion


		#region IFullscreenClient implementation
	
		public void CreateFullscreenAdView ()
		{
			Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
		}

		public void ShowFullscreenAdView ()
		{
			Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
		}

		public void DestroyFullscreenAdView ()
		{
			Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
		}
		#endregion

		#region IRwdVideoClient implementation
		public void CreateRwdVideoView ()
		{
			Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
		}
			
		public void ShowRwdVideoView ()
		{
			Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
		}
			
		public void DestroyRwdVideoView ()
		{
			Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
		}
		#endregion
	}
}