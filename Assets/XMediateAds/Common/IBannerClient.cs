﻿
using UnityEngine;
using System;
using XMediateAds.Api;
using XMediateAds.Api.AdSettings;

namespace XMediateAds.Common
{
	public interface IBannerClient
	{
		// Ad event fired when the banner ad has been received.
		event EventHandler<EventArgs> OnBannerLoaded;
		// Ad event fired when the banner ad has failed to load.
		event EventHandler<AdFailedToLoadEventArgs> OnBannerFailedToLoad;
		// Ad event fired when the banner ad is expanded.
		event EventHandler<EventArgs> OnBannerExpanded;
		// Ad event fired when the banner ad is clicked.
		event EventHandler<EventArgs> OnBannerClicked;
		// Ad event fired when the banner ad is collapsed.
		event EventHandler<EventArgs> OnBannerCollapsed;
		// Ad event fired when the banner ad is leaving the application.
		event EventHandler<EventArgs> OnLeaveApplication;

		// Creates a banner view and adds it to the view hierarchy.
		void CreateBannerView(XmBannerType bannerType, AdPosition position);

		// Creates a banner view and adds it to the view hierarchy with a custom position.
		void CreateBannerView(XmBannerType bannerType, int x, int y);

		// Requests a new ad for the banner view.
		void LoadAd(XmAdSettings adSettings);

		// Destroys a banner view.
		void DestroyBannerView();
	}
}
