﻿using UnityEditor;
using UnityEngine;
using XMediateAds;

namespace XMediateAds
{
    [CustomEditor(typeof(XMediateInitializer)), CanEditMultipleObjects]
    class XMediateInitializerEditor : Editor
    {
        SerializedProperty androidPublisherId;
        SerializedProperty androidApplicationId;

        SerializedProperty iosPublisherId;
        SerializedProperty iosApplicationId;
        
        void OnEnable()
        {
            androidPublisherId = serializedObject.FindProperty("androidPublisherIdentifier");
            androidApplicationId = serializedObject.FindProperty("androidApplicationIdentifier");

            iosPublisherId = serializedObject.FindProperty("iOSPublisherIdentifier");
            iosApplicationId = serializedObject.FindProperty("iOSApplicationIdentifier");
        }

        public override void OnInspectorGUI()
        {
            // Show the custom GUI controls
            EditorGUILayout.Separator();

            EditorGUILayout.LabelField("Android Credentials", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(androidPublisherId, new GUIContent("Publisher Identifier: "));
            EditorGUILayout.PropertyField(androidApplicationId, new GUIContent("Application Identifier: "));

            EditorGUILayout.Separator();

            EditorGUILayout.LabelField("iOS Credentials", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(iosPublisherId, new GUIContent("Publisher Identifier:  "));
            EditorGUILayout.PropertyField(iosApplicationId, new GUIContent("Application Identifier: "));

            EditorGUILayout.Separator();

            // Apply changes to the serializedProperty
            serializedObject.ApplyModifiedProperties();
        }


    }
}

