﻿using UnityEngine;
using XMediateAds.Api;

namespace XMediateAds
{
    public class XMediateInitializer : MonoBehaviour
    {
        public string androidPublisherIdentifier;
        public string androidApplicationIdentifier;

        public string iOSPublisherIdentifier;
        public string iOSApplicationIdentifier;

        public void Start()
        {
        #if UNITY_EDITOR
            string publisherIdentifier = "1";
            string applicationIdentifier = "1";
        #elif UNITY_ANDROID
	    	string publisherIdentifier = androidPublisherIdentifier;
	    	string applicationIdentifier = androidApplicationIdentifier;
        #elif UNITY_IOS
		    string publisherIdentifier = iOSPublisherIdentifier;
		    string applicationIdentifier = iOSApplicationIdentifier;
        #else
		    string publisherIdentifier = "*******";
		    string applicationIdentifier = "*******";
        #endif

            XMediate.init(publisherIdentifier, applicationIdentifier);
        }
    }
}
